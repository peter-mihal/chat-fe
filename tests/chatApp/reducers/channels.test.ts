import {channels} from '../../../src/chatApp/reducers/channels';
import {
  CHAT_APP_CHANNEL_CHANGE_ORDER,
  CHAT_APP_CHANNEL_CREATE_SUCCESS,
  CHAT_APP_CHANNEL_DELETE_SUCCESS,
  CHAT_APP_CHANNEL_UPDATE_SUCCESS,
  CHAT_APP_CHANNELS_LOADING_SUCCESS
} from '../../../src/chatApp/constants/actionTypes';
import * as Immutable from 'immutable';
import {UP} from '../../../src/chatApp/constants/changeOrderDirections';

const mockPrevState = {
  allIds: Immutable.List(['2', '1']),
  byId: Immutable.Map({
    1: {
      id: '1',
      name: 'Channel1',
      customData: {
        users: Immutable.Set(['mail@mail.com'])
      }
    },
    2: {
      id: '2',
      name: 'Channel2',
      customData: {
        users: Immutable.Set(['mail@mail.com', 'someone@else.com'])
      }
    }
  })
};

describe('channels reducer', () => {

  test('no action', () => {
    const newState = channels(mockPrevState, {type: 'nothing'});
    expect(newState).toBe(mockPrevState);
  });

  test('loading success', () => {
    const newState = channels(mockPrevState, {
      type: CHAT_APP_CHANNELS_LOADING_SUCCESS,
      payload: {
        channels: [
          {
            id: '3',
            name: 'Channel3',
            customData: {
              users: Immutable.Set(['mail@mail.com', 'someone@else.com'])
            }
          }
        ]
      }
    });
    expect(newState.allIds.size).toBe(1);
    expect(newState.allIds.get(0)).toBe('3');
    expect(newState.byId.size).toBe(1);
    expect(newState.byId.get('3').name).toBe('Channel3');
  });

  test('delete success', () => {
    const newState = channels(mockPrevState, {
      type: CHAT_APP_CHANNEL_DELETE_SUCCESS,
      payload: {
        channels: [
          {
            id: '3',
            name: 'Channel3',
            customData: {
              users: Immutable.Set(['mail@mail.com', 'someone@else.com'])
            }
          }
        ]
      }
    });
    expect(newState.allIds.size).toBe(1);
    expect(newState.allIds.get(0)).toBe('3');
    expect(newState.byId.size).toBe(1);
    expect(newState.byId.get('3').name).toBe('Channel3');
  });

  test('create success', () => {
    const newState = channels(mockPrevState, {
      type: CHAT_APP_CHANNEL_CREATE_SUCCESS,
      payload: {
        channel: {
          id: '3',
          name: 'Channel3',
          customData: {
            users: Immutable.Set(['mail@mail.com', 'someone@else.com'])
          }
        }
      }
    });
    expect(newState.allIds.size).toBe(3);
    expect(newState.allIds.get(2)).toBe('3');
    expect(newState.byId.size).toBe(3);
    expect(newState.byId.get('3').name).toBe('Channel3');
  });

  test('update success', () => {
    const newState = channels(mockPrevState, {
      type: CHAT_APP_CHANNEL_UPDATE_SUCCESS,
      payload: {
        channel: {
          id: '1',
          name: 'Channel3',
          customData: {
            users: Immutable.Set(['mail@mail.com', 'someone@else.com'])
          }
        }
      }
    });
    expect(newState.allIds.size).toBe(2);
    expect(newState.allIds.get(0)).toBe('2');
    expect(newState.byId.size).toBe(2);
    expect(newState.byId.get('1').name).toBe('Channel3');
    expect(newState.byId.get('1').customData.users.contains('mail@mail.com')).toBeTruthy();
    expect(newState.byId.get('1').customData.users.contains('someone@else.com')).toBeTruthy();
  });

  test('change order', () => {
    const newState = channels(mockPrevState, {
      type: CHAT_APP_CHANNEL_CHANGE_ORDER,
      payload: {
        index: 1,
        direction: UP
      }
    });
    expect(newState.allIds.size).toBe(2);
    expect(newState.allIds.get(0)).toBe('1');
    expect(newState.allIds.get(1)).toBe('2');
    expect(newState.byId.size).toBe(2);
    expect(newState.byId.get('1').name).toBe('Channel1');
    expect(newState.byId.get('2').name).toBe('Channel2');
  });
});
