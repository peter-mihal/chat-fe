import {messages} from '../../../src/chatApp/reducers/messages';
import {
  CHAT_APP_MESSAGE_CREATE_SUCCESS,
  CHAT_APP_MESSAGE_DELETE_COMPLETE_SUCCESS,
  CHAT_APP_MESSAGE_UPDATE_SUCCESS,
  CHAT_APP_MESSAGES_LOADING_SUCCESS
} from '../../../src/chatApp/constants/actionTypes';
import * as Immutable from 'immutable';

const mockPrevState = {
  allIds: Immutable.List(['2', '1']),
  byId: Immutable.Map({
    1: {
      id: '1',
      value: 'hello world',
      createdAt: '2018-12-20T15:44:55.479Z',
      createdBy: 'mail@mail.com',
      updatedAt: '2018-12-20T15:44:55.479Z',
      updatedBy: 'mail@mail.com',
      customData: {
        likes: 0,
        dislikes: 0,
        file: undefined,
        channelId: 'cid',
        userPhoto: 'photo'
      }
    },
    2: {
      id: '2',
      value: 'Hello!',
      createdAt: '2018-12-19T15:44:55.479Z',
      createdBy: 'someone@else.com',
      updatedAt: '2018-12-19T15:44:55.479Z',
      updatedBy: 'someone@else.com',
      customData: {
        likes: 1,
        dislikes: 0,
        file: undefined,
        channelId: 'cid',
        userPhoto: 'photo'
      }
    }
  })
};

describe('messages reducer', () => {

  test('no action', () => {
    const newState = messages(mockPrevState, {type: 'nothing'});
    expect(newState).toBe(mockPrevState);
  });

  test('loading success', () => {
    const newState = messages(mockPrevState, {
      type: CHAT_APP_MESSAGES_LOADING_SUCCESS,
      payload: {
        messages: [
          {
            id: '3',
            value: 'hi world',
            createdAt: '2018-12-20T15:44:55.479Z',
            createdBy: 'mail@mail.com',
            updatedAt: '2018-12-20T15:44:55.479Z',
            updatedBy: 'mail@mail.com',
            customData: {
              likes: 0,
              dislikes: 0,
              file: undefined,
              channelId: 'cid',
              userPhoto: 'photo'
            }
          },
          {
            id: '4',
            value: 'Hi!',
            createdAt: '2018-12-19T15:44:55.479Z',
            createdBy: 'someone@else.com',
            updatedAt: '2018-12-19T15:44:55.479Z',
            updatedBy: 'someone@else.com',
            customData: {
              likes: 1,
              dislikes: 0,
              file: undefined,
              channelId: 'cid',
              userPhoto: 'photo'
            }
          }
        ]
      }
    });
    expect(newState.allIds.size).toBe(2);
    expect(newState.allIds.get(0)).toBe('4');
    expect(newState.allIds.get(1)).toBe('3');
    expect(newState.byId.size).toBe(2);
    expect(newState.byId.get('3').value).toBe('hi world');
    expect(newState.byId.get('4').value).toBe('Hi!');
  });

  test('delete success', () => {
    const newState = messages(mockPrevState, {
      type: CHAT_APP_MESSAGE_DELETE_COMPLETE_SUCCESS,
      payload: {
        messages: [
          {
            id: '3',
            value: 'hi world',
            createdAt: '2018-12-20T15:44:55.479Z',
            createdBy: 'mail@mail.com',
            updatedAt: '2018-12-20T15:44:55.479Z',
            updatedBy: 'mail@mail.com',
            customData: {
              likes: 0,
              dislikes: 0,
              file: undefined,
              channelId: 'cid',
              userPhoto: 'photo'
            }
          },
          {
            id: '4',
            value: 'Hi!',
            createdAt: '2018-12-19T15:44:55.479Z',
            createdBy: 'someone@else.com',
            updatedAt: '2018-12-19T15:44:55.479Z',
            updatedBy: 'someone@else.com',
            customData: {
              likes: 1,
              dislikes: 0,
              file: undefined,
              channelId: 'cid',
              userPhoto: 'photo'
            }
          }
        ]
      }
    });
    expect(newState.allIds.size).toBe(2);
    expect(newState.allIds.get(0)).toBe('4');
    expect(newState.allIds.get(1)).toBe('3');
    expect(newState.byId.size).toBe(2);
    expect(newState.byId.get('3').value).toBe('hi world');
    expect(newState.byId.get('4').value).toBe('Hi!');
  });

  test('create success', () => {
    const newState = messages(mockPrevState, {
      type: CHAT_APP_MESSAGE_CREATE_SUCCESS,
      payload: {
        message: {
          id: '3',
          value: 'Hi!',
          createdAt: '2018-12-21T15:44:55.479Z',
          createdBy: 'someone@else.com',
          updatedAt: '2018-12-21T15:44:55.479Z',
          updatedBy: 'someone@else.com',
          customData: {
            likes: 0,
            dislikes: 0,
            file: undefined,
            channelId: 'cid',
            userPhoto: 'photo'
          }
        }
      }
    });
    expect(newState.allIds.size).toBe(3);
    expect(newState.allIds.get(2)).toBe('3');
    expect(newState.byId.size).toBe(3);
    expect(newState.byId.get('3').value).toBe('Hi!');
  });

  test('update success', () => {
    const newState = messages(mockPrevState, {
      type: CHAT_APP_MESSAGE_UPDATE_SUCCESS,
      payload: {
        message: {
          id: '2',
          value: 'Hi!',
          createdAt: '2018-12-19T15:44:55.479Z',
          createdBy: 'someone@else.com',
          updatedAt: '2018-12-21T15:44:55.479Z',
          updatedBy: 'someone@else.com',
          customData: {
            likes: 2,
            dislikes: 1,
            file: undefined,
            channelId: 'cid',
            userPhoto: 'photo'
          }
        }
      }
    });
    expect(newState.allIds.size).toBe(2);
    expect(newState.allIds.get(0)).toBe('2');
    expect(newState.byId.size).toBe(2);
    expect(newState.byId.get('2').value).toBe('Hi!');
    expect(newState.byId.get('2').customData.likes).toBe(2);
    expect(newState.byId.get('2').customData.dislikes).toBe(1);
  });
});
