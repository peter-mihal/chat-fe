import {isLoading} from '../../../src/chatApp/reducers/isLoading';
import {
  CHAT_APP_CHANNELS_LOADING_STARTED,
  CHAT_APP_CHANNELS_LOADING_SUCCESS,
  CHAT_APP_MESSAGES_LOADING_STARTED,
  CHAT_APP_MESSAGES_LOADING_SUCCESS
} from '../../../src/chatApp/constants/actionTypes';

describe('is loading', () => {

  test('no action', () => {
    const newState = isLoading(false, {type: 'nothing'});
    expect(newState).toBe(false);
  });

  test('loading messages started', () => {
    const newState = isLoading(false, {type: CHAT_APP_MESSAGES_LOADING_STARTED});
    expect(newState).toBe(true);
  });

  test('loading channels started', () => {
    const newState = isLoading(false, {type: CHAT_APP_CHANNELS_LOADING_STARTED});
    expect(newState).toBe(true);
  });

  test('loading messages success', () => {
    const newState = isLoading(true, {type: CHAT_APP_MESSAGES_LOADING_SUCCESS});
    expect(newState).toBe(false);
  });

  test('loading channels success', () => {
    const newState = isLoading(true, {type: CHAT_APP_CHANNELS_LOADING_SUCCESS});
    expect(newState).toBe(false);
  });
});
