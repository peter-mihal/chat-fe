import {isSaving} from '../../../src/chatApp/reducers/isSaving';
import {
  CHAT_APP_CHANNEL_CREATE_STARTED,
  CHAT_APP_CHANNEL_CREATE_SUCCESS,
  CHAT_APP_CHANNEL_DELETE_STARTED,
  CHAT_APP_CHANNEL_DELETE_SUCCESS,
  CHAT_APP_CHANNEL_UPDATE_STARTED,
  CHAT_APP_CHANNEL_UPDATE_SUCCESS,
  CHAT_APP_MESSAGE_CREATE_STARTED,
  CHAT_APP_MESSAGE_CREATE_SUCCESS,
  CHAT_APP_MESSAGE_DELETE_COMPLETE_STARTED,
  CHAT_APP_MESSAGE_DELETE_COMPLETE_SUCCESS,
  CHAT_APP_MESSAGE_UPDATE_STARTED,
  CHAT_APP_MESSAGE_UPDATE_SUCCESS
} from '../../../src/chatApp/constants/actionTypes';

describe('is saving', () => {

  test('no action', () => {
    const newState = isSaving(false, {type: 'nothing'});
    expect(newState).toBe(false);
  });

  test('message create started', () => {
    const newState = isSaving(false, {type: CHAT_APP_MESSAGE_CREATE_STARTED});
    expect(newState).toBe(true);
  });

  test('channel create started', () => {
    const newState = isSaving(false, {type: CHAT_APP_CHANNEL_CREATE_STARTED});
    expect(newState).toBe(true);
  });

  test('message update started', () => {
    const newState = isSaving(false, {type: CHAT_APP_MESSAGE_UPDATE_STARTED});
    expect(newState).toBe(true);
  });

  test('channel update started', () => {
    const newState = isSaving(false, {type: CHAT_APP_CHANNEL_UPDATE_STARTED});
    expect(newState).toBe(true);
  });

  test('message delete started', () => {
    const newState = isSaving(false, {type: CHAT_APP_MESSAGE_DELETE_COMPLETE_STARTED});
    expect(newState).toBe(true);
  });

  test('channel delete started', () => {
    const newState = isSaving(false, {type: CHAT_APP_CHANNEL_DELETE_STARTED});
    expect(newState).toBe(true);
  });

  test('message create success', () => {
    const newState = isSaving(true, {type: CHAT_APP_MESSAGE_CREATE_SUCCESS});
    expect(newState).toBe(false);
  });

  test('channel create success', () => {
    const newState = isSaving(true, {type: CHAT_APP_CHANNEL_CREATE_SUCCESS});
    expect(newState).toBe(false);
  });

  test('message update success', () => {
    const newState = isSaving(true, {type: CHAT_APP_MESSAGE_UPDATE_SUCCESS});
    expect(newState).toBe(false);
  });

  test('channel update success', () => {
    const newState = isSaving(true, {type: CHAT_APP_CHANNEL_UPDATE_SUCCESS});
    expect(newState).toBe(false);
  });

  test('message delete success', () => {
    const newState = isSaving(true, {type: CHAT_APP_MESSAGE_DELETE_COMPLETE_SUCCESS});
    expect(newState).toBe(false);
  });

  test('channel delete success', () => {
    const newState = isSaving(true, {type: CHAT_APP_CHANNEL_DELETE_SUCCESS});
    expect(newState).toBe(false);
  });
});
