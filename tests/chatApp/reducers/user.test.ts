import {user} from '../../../src/chatApp/reducers/user';
import {
  CHAT_APP_GET_USER_SUCCESS,
  CHAT_APP_LOGIN_SUCCESS,
  CHAT_APP_LOGOUT_SUCCESS,
  CHAT_APP_USER_PROFILE_UPDATE_SUCCESS
} from '../../../src/chatApp/constants/actionTypes';

const mockPrevState = {
  username: '',
  email: '',
  isLoggedIn: false,
  customData: {
    username: '',
    photoUri: ''
  }
};

describe('user reducer', () => {

  test('no action', () => {
    const newState = user(mockPrevState, {type: 'nothing'});
    expect(newState).toBe(mockPrevState);
  });

  test('login success', () => {
    const newState = user(mockPrevState, {
      type: CHAT_APP_LOGIN_SUCCESS,
      payload: {
        user: {
          username: 'adam',
          email: 'mail@mail.com',
          isLoggedIn: true,
          customData: {
            username: 'adam',
            photoUri: undefined
          }
        }
      }
    });
    expect(newState.username).toBe('adam');
    expect(newState.email).toBe('mail@mail.com');
    expect(newState.isLoggedIn).toBe(true);
    expect(newState.customData.username).toBe('adam');
    expect(newState.customData.photoUri).toBe(undefined);
  });

  test('logout success', () => {
    const newState = user(mockPrevState, {
      type: CHAT_APP_LOGOUT_SUCCESS,
      payload: {
        user: {
          username: 'adam',
          email: 'mail@mail.com',
          isLoggedIn: true,
          customData: {
            username: 'adam',
            photoUri: undefined
          }
        }
      }
    });
    expect(newState.username).toBe('adam');
    expect(newState.email).toBe('mail@mail.com');
    expect(newState.isLoggedIn).toBe(true);
    expect(newState.customData.username).toBe('adam');
    expect(newState.customData.photoUri).toBe(undefined);
  });

  test('update profile success', () => {
    const newState = user(mockPrevState, {
      type: CHAT_APP_USER_PROFILE_UPDATE_SUCCESS,
      payload: {
        user: {
          username: 'adam',
          email: 'mail@mail.com',
          isLoggedIn: true,
          customData: {
            username: 'adam',
            photoUri: undefined
          }
        }
      }
    });
    expect(newState.username).toBe('adam');
    expect(newState.email).toBe('mail@mail.com');
    expect(newState.isLoggedIn).toBe(true);
    expect(newState.customData.username).toBe('adam');
    expect(newState.customData.photoUri).toBe(undefined);
  });

  test('get user success', () => {
    const newState = user(mockPrevState, {
      type: CHAT_APP_GET_USER_SUCCESS,
      payload: {
        user: {
          username: 'adam',
          email: 'mail@mail.com',
          isLoggedIn: true,
          customData: {
            username: 'adam',
            photoUri: undefined
          }
        }
      }
    });
    expect(newState.username).toBe('adam');
    expect(newState.email).toBe('mail@mail.com');
    expect(newState.isLoggedIn).toBe(true);
    expect(newState.customData.username).toBe('adam');
    expect(newState.customData.photoUri).toBe(undefined);
  });
});
