import {messagesFilter} from '../../../src/chatApp/reducers/filterMessages';
import {
  CHAT_APP_MESSAGES_FILTER
} from '../../../src/chatApp/constants/actionTypes';

describe('filter messages', () => {

  test('no action', () => {
    const newState = messagesFilter('', {type: 'nothing'});
    expect(newState).toBe('');
  });

  test('filter', () => {
    const newState = messagesFilter('', {
      type: CHAT_APP_MESSAGES_FILTER,
      payload: {
        filter: 'filter'
      }
    });
    expect(newState).toBe('filter');
  });
});
