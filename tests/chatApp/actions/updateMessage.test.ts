import { dislikeMessageFactory, likeMessageFactory } from '../../../src/chatApp/actions/updateMessage';
import {
  CHAT_APP_MESSAGE_UPDATE_STARTED,
  CHAT_APP_MESSAGE_UPDATE_SUCCESS
} from '../../../src/chatApp/constants/actionTypes';
import * as Immutable from 'immutable';

class MockApiHelper {
  public get = jest.fn();
  public post = jest.fn();
  public put = jest.fn(_ => ({
    id: 'id',
    value: 'hello world',
    createdAt: '2018-12-20T15:44:55.479Z',
    createdBy: 'mail@mail.com',
    updatedAt: '2018-12-20T15:44:55.479Z',
    updatedBy: 'mail@mail.com',
    customData: {
      likes: 0,
      dislikes: 0,
      file: undefined,
      channelId: 'cid'
    }
  }));
  public delete = jest.fn();
}

const mockGetState = () => {
  return {
    chatApp: {
      messages: {
        byId: Immutable.Map({
          id: {
            id: 'id',
            value: 'hello world',
            createdAt: '2018-12-20T15:44:55.479Z',
            createdBy: 'mail@mail.com',
            updatedAt: '2018-12-20T15:44:55.479Z',
            updatedBy: 'mail@mail.com',
            customData: {
              likes: 1,
              dislikes: 1,
              file: undefined,
              channelId: 'cid'
            }
          }
        })
      }
    }
  };
};

describe('like', () => {
  let dispatch = jest.fn();
  let mockApiHelper: MockApiHelper;

  beforeEach(() => {
    dispatch = jest.fn();
    mockApiHelper = new MockApiHelper();
  });

  test('Dispatch count', async () => {
    await likeMessageFactory(mockApiHelper)('id')(dispatch, mockGetState);
    expect(dispatch.mock.calls.length).toBe(2);
  });

  test('Dispatch types', async () => {
    await likeMessageFactory(mockApiHelper)('id')(dispatch, mockGetState);
    expect(dispatch.mock.calls[0][0].type).toBe(CHAT_APP_MESSAGE_UPDATE_STARTED);
    expect(dispatch.mock.calls[1][0].type).toBe(CHAT_APP_MESSAGE_UPDATE_SUCCESS);
  });

  test('Payload type', async () => {
    await likeMessageFactory(mockApiHelper)('id')(dispatch, mockGetState);
    expect(dispatch.mock.calls[1][0].payload).toHaveProperty('message');
  });

  test('Called api method', async () => {
    await likeMessageFactory(mockApiHelper)('id')(dispatch, mockGetState);
    expect(mockApiHelper.put.mock.calls.length).toBe(1);
  });

  test('Parameters of called api method', async () => {
    await likeMessageFactory(mockApiHelper)('id')(dispatch, mockGetState);
    expect(mockApiHelper.put.mock.calls[0][1].id).toBe('id');
    expect(mockApiHelper.put.mock.calls[0][1].value).toBe('hello world');
    expect(mockApiHelper.put.mock.calls[0][1].customData.likes).toBe(2);
    expect(mockApiHelper.put.mock.calls[0][1].customData.dislikes).toBe(1);
  });
});

describe('dislike', () => {
  let dispatch = jest.fn();
  let mockApiHelper: MockApiHelper;

  beforeEach(() => {
    dispatch = jest.fn();
    mockApiHelper = new MockApiHelper();
  });

  test('Dispatch count', async () => {
    await dislikeMessageFactory(mockApiHelper)('id')(dispatch, mockGetState);
    expect(dispatch.mock.calls.length).toBe(2);
  });

  test('Dispatch types', async () => {
    await dislikeMessageFactory(mockApiHelper)('id')(dispatch, mockGetState);
    expect(dispatch.mock.calls[0][0].type).toBe(CHAT_APP_MESSAGE_UPDATE_STARTED);
    expect(dispatch.mock.calls[1][0].type).toBe(CHAT_APP_MESSAGE_UPDATE_SUCCESS);
  });

  test('Payload type', async () => {
    await dislikeMessageFactory(mockApiHelper)('id')(dispatch, mockGetState);
    expect(dispatch.mock.calls[1][0].payload).toHaveProperty('message');
  });

  test('Called api method', async () => {
    await dislikeMessageFactory(mockApiHelper)('id')(dispatch, mockGetState);
    expect(mockApiHelper.put.mock.calls.length).toBe(1);
  });

  test('Parameters of called api method', async () => {
    await dislikeMessageFactory(mockApiHelper)('id')(dispatch, mockGetState);
    expect(mockApiHelper.put.mock.calls[0][1].id).toBe('id');
    expect(mockApiHelper.put.mock.calls[0][1].value).toBe('hello world');
    expect(mockApiHelper.put.mock.calls[0][1].customData.likes).toBe(1);
    expect(mockApiHelper.put.mock.calls[0][1].customData.dislikes).toBe(2);
  });
});
