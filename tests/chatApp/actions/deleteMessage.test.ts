import * as Immutable from 'immutable';
import { deleteMessageFactory } from '../../../src/chatApp/actions/deleteMessage';
import {
  CHAT_APP_MESSAGE_DELETE_COMPLETE_STARTED,
  CHAT_APP_MESSAGE_DELETE_COMPLETE_SUCCESS
} from '../../../src/chatApp/constants/actionTypes';

class MockApiHelper {
  public get = jest.fn(_ => ([]));
  public post = jest.fn();
  public put = jest.fn();
  public delete = jest.fn();
}

const mockGetState = () => {
  return {
    chatApp: {
      messages: {
        byId: Immutable.Map({
          id: {
            id: 'id',
            value: 'hello world',
            createdAt: '2018-12-20T15:44:55.479Z',
            createdBy: 'mail@mail.com',
            updatedAt: '2018-12-20T15:44:55.479Z',
            updatedBy: 'mail@mail.com',
            customData: {
              likes: 0,
              dislikes: 0,
              file: undefined,
              channelId: 'cid'
            }
          }
        })
      }
    }
  };
};

describe('deleteMessage', () => {
  let dispatch = jest.fn();
  let mockApiHelper: MockApiHelper;

  beforeEach(() => {
    dispatch = jest.fn();
    mockApiHelper = new MockApiHelper();
  });

  test('Dispatch count', async () => {
    await deleteMessageFactory(mockApiHelper)('id')(dispatch, mockGetState);
    expect(dispatch.mock.calls.length).toBe(2);
  });

  test('Dispatch types', async () => {
    await deleteMessageFactory(mockApiHelper)('id')(dispatch, mockGetState);
    expect(dispatch.mock.calls[0][0].type).toBe(CHAT_APP_MESSAGE_DELETE_COMPLETE_STARTED);
    expect(dispatch.mock.calls[1][0].type).toBe(CHAT_APP_MESSAGE_DELETE_COMPLETE_SUCCESS);
  });

  test('Payload type', async () => {
    await deleteMessageFactory(mockApiHelper)('id')(dispatch, mockGetState);
    expect(dispatch.mock.calls[1][0].payload).toHaveProperty('messages');
  });

  test('Called api method', async () => {
    await deleteMessageFactory(mockApiHelper)('id')(dispatch, mockGetState);
    expect(mockApiHelper.delete.mock.calls.length).toBe(1);
  });
});
