import {createFileActionFactory, createMessageActionFactory} from '../../../src/chatApp/actions/createMessage';
import {
  CHAT_APP_FILE_CREATE_SUCCESS,
  CHAT_APP_MESSAGE_CREATE_STARTED,
  CHAT_APP_MESSAGE_CREATE_SUCCESS
} from '../../../src/chatApp/constants/actionTypes';

class MockApiHelperFile {
  public get = jest.fn(_ => ({
    fileUri: 'u/r/i'
  }));
  public post = jest.fn(_ => ([{
    id: 'fid',
    name: 'picture',
    extension: 'jpg',
    createdBy: 'mail@mail.com',
    fileSize: 100
  }]));
  public put = jest.fn();
  public delete = jest.fn();
}

class MockApiHelperMessage {
  public get = jest.fn(_ => ({
    id: 'id',
    value: 'hello world',
    createdAt: '2018-12-20T15:44:55.479Z',
    createdBy: 'mail@mail.com',
    updatedAt: '2018-12-20T15:44:55.479Z',
    updatedBy: 'mail@mail.com',
    customData: {
      likes: 0,
      dislikes: 0,
      file: undefined,
      channelId: 'cid'
    }
  }));
  public post = jest.fn(_ => ({
    id: 'id',
    value: 'hello world',
    createdAt: '2018-12-20T15:44:55.479Z',
    createdBy: 'mail@mail.com',
    updatedAt: '2018-12-20T15:44:55.479Z',
    updatedBy: 'mail@mail.com',
    customData: {}
  }));
  public put = jest.fn();
  public delete = jest.fn();
}

const user = {
  email: 'mail@mail.com',
  username: 'adam',
  isLoggedIn: true,
  customData: {
    username: 'adam',
    photoUri: undefined
  }
};

const file = {
  createdBy: 'mail@mail.com',
  extension: 'jpg',
  fileSize: 100,
  id: 'fid',
  name: 'picture',
  fileUri: 'u/r/i'
};

describe('createMessage', () => {
  let dispatch = jest.fn();
  let mockApiHelper: MockApiHelperMessage;

  beforeEach(() => {
    dispatch = jest.fn();
    mockApiHelper = new MockApiHelperMessage();
  });

  test('Dispatch count', async () => {
    await createMessageActionFactory(mockApiHelper)('hello world', user, 'cid')(dispatch);
    expect(dispatch.mock.calls.length).toBe(2);
  });

  test('Dispatch types', async () => {
    await createMessageActionFactory(mockApiHelper)('hello world', user, 'cid')(dispatch);
    expect(dispatch.mock.calls[0][0].type).toBe(CHAT_APP_MESSAGE_CREATE_STARTED);
    expect(dispatch.mock.calls[1][0].type).toBe(CHAT_APP_MESSAGE_CREATE_SUCCESS);
  });

  test('Payload type', async () => {
    await createMessageActionFactory(mockApiHelper)('hello world', user, 'cid')(dispatch);
    expect(dispatch.mock.calls[1][0].payload).toHaveProperty('message');
  });

  test('Called api method', async () => {
    await createMessageActionFactory(mockApiHelper)('hello world', user, 'cid')(dispatch);
    expect(mockApiHelper.post.mock.calls.length).toBe(1);
  });

  test('Parameters of called api method', async () => {
    await createMessageActionFactory(mockApiHelper)('hello world', user, 'cid', file)(dispatch);
    expect(mockApiHelper.post.mock.calls[0][1].value).toBe('hello world');
    expect(mockApiHelper.post.mock.calls[0][1].createdBy).toBe('mail@mail.com');
    expect(mockApiHelper.post.mock.calls[0][1].customData.file).toBe(file);
    expect(mockApiHelper.post.mock.calls[0][1].customData.channelId).toBe('cid');
  });
});

describe('createFile', () => {
  let dispatch = jest.fn();
  let mockApiHelper: MockApiHelperFile;

  beforeEach(() => {
    dispatch = jest.fn();
    mockApiHelper = new MockApiHelperFile();
  });

  test('Dispatch count', async () => {
    await createFileActionFactory(mockApiHelper)('file')(dispatch);
    expect(dispatch.mock.calls.length).toBe(1);
  });

  test('Dispatch types', async () => {
    await createFileActionFactory(mockApiHelper)('file')(dispatch);
    expect(dispatch.mock.calls[0][0].type).toBe(CHAT_APP_FILE_CREATE_SUCCESS);
  });

  test('Called api method', async () => {
    await createFileActionFactory(mockApiHelper)('file')(dispatch);
    expect(mockApiHelper.post.mock.calls.length).toBe(1);
  });

  test('Parameters of called api method', async () => {
    await createFileActionFactory(mockApiHelper)('file')(dispatch);
    expect(mockApiHelper.post.mock.calls[0][1].get('Files')).toBe('file');
  });
});
