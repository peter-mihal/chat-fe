import { deleteChannelFactory } from '../../../src/chatApp/actions/deleteChannel';
import {
  CHAT_APP_CHANNEL_DELETE_STARTED,
  CHAT_APP_CHANNEL_DELETE_SUCCESS
} from '../../../src/chatApp/constants/actionTypes';

class MockApiHelper {
  public get = jest.fn(_ => ([]));
  public post = jest.fn();
  public put = jest.fn();
  public delete = jest.fn();
}

describe('deleteChannel', () => {
  let dispatch = jest.fn();
  let mockApiHelper: MockApiHelper;

  beforeEach(() => {
    dispatch = jest.fn();
    mockApiHelper = new MockApiHelper();
  });

  test('Dispatch count', async () => {
    await deleteChannelFactory(mockApiHelper)('id', 'mail@mail.com')(dispatch);
    expect(dispatch.mock.calls.length).toBe(2);
  });

  test('Dispatch types', async () => {
    await deleteChannelFactory(mockApiHelper)('id', 'mail@mail.com')(dispatch);
    expect(dispatch.mock.calls[0][0].type).toBe(CHAT_APP_CHANNEL_DELETE_STARTED);
    expect(dispatch.mock.calls[1][0].type).toBe(CHAT_APP_CHANNEL_DELETE_SUCCESS);
  });

  test('Payload type', async () => {
    await deleteChannelFactory(mockApiHelper)('id', 'mail@mail.com')(dispatch);
    expect(dispatch.mock.calls[1][0].payload).toHaveProperty('channels');
  });

  test('Called api method', async () => {
    await deleteChannelFactory(mockApiHelper)('id', 'mail@mail.com')(dispatch);
    expect(mockApiHelper.delete.mock.calls.length).toBe(1);
  });
});
