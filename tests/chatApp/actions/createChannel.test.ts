import { createChannelFactory } from '../../../src/chatApp/actions/createChannel';
import {
  CHAT_APP_CHANNEL_CREATE_STARTED,
  CHAT_APP_CHANNEL_CREATE_SUCCESS
} from '../../../src/chatApp/constants/actionTypes';

class MockApiHelper {
  public get = jest.fn();
  public post = jest.fn(_ => ({
    id: 'id',
    name: 'Channel',
    customData: {
      users: ['mail@mail.com']
    }
  }));
  public put = jest.fn();
  public delete = jest.fn();
}

describe('createChannel', () => {
  let dispatch = jest.fn();
  let mockApiHelper: MockApiHelper;

  beforeEach(() => {
    dispatch = jest.fn();
    mockApiHelper = new MockApiHelper();
  });

  test('Dispatch count', async () => {
    await createChannelFactory(mockApiHelper)('Channel', 'mail@mail.com')(dispatch);
    expect(dispatch.mock.calls.length).toBe(2);
  });

  test('Dispatch types', async () => {
    await createChannelFactory(mockApiHelper)('Channel', 'mail@mail.com')(dispatch);
    expect(dispatch.mock.calls[0][0].type).toBe(CHAT_APP_CHANNEL_CREATE_STARTED);
    expect(dispatch.mock.calls[1][0].type).toBe(CHAT_APP_CHANNEL_CREATE_SUCCESS);
  });

  test('Payload type', async () => {
    await createChannelFactory(mockApiHelper)('Channel', 'mail@mail.com')(dispatch);
    expect(dispatch.mock.calls[1][0].payload).toHaveProperty('channel');
  });

  test('Called api method', async () => {
    await createChannelFactory(mockApiHelper)('Channel', 'mail@mail.com')(dispatch);
    expect(mockApiHelper.post.mock.calls.length).toBe(1);
  });

  test('Parameters of called api method', async () => {
    await createChannelFactory(mockApiHelper)('Channel', 'mail@mail.com')(dispatch);
    expect(mockApiHelper.post.mock.calls[0][1].name).toBe('Channel');
    expect(mockApiHelper.post.mock.calls[0][1].customData.users).toContain('mail@mail.com');
  });
});
