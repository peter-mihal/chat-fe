import { changeChannelNameFactory, inviteMemberToChannelFactory } from '../../../src/chatApp/actions/updateChannel';
import {
  CHAT_APP_CHANNEL_UPDATE_STARTED,
  CHAT_APP_CHANNEL_UPDATE_SUCCESS
} from '../../../src/chatApp/constants/actionTypes';
import * as Immutable from 'immutable';

class MockApiHelperChangeName {
  public get = jest.fn();
  public post = jest.fn();
  public put = jest.fn(_ => ({
    id: 'id',
    name: 'Channel2',
    customData: {
      users: ['mail@mail.com']
    }
  }));
  public delete = jest.fn();
}

class MockApiHelperInviteMember {
  public get = jest.fn();
  public post = jest.fn();
  public put = jest.fn(_ => ({
    id: 'id',
    name: 'Channel',
    customData: {
      users: ['mail@mail.com', 'someone@else.com']
    }
  }));
  public delete = jest.fn();
}

const mockGetState = () => {
  return {
    chatApp: {
      channels: {
        byId: Immutable.Map({
          id: {
            id: 'id',
            name: 'Channel',
            customData: {
              users: ['mail@mail.com']
            }
          }
        })
      }
    }
  };
};

describe('changeChannelName', () => {
  let dispatch = jest.fn();
  let mockApiHelper: MockApiHelperChangeName;

  beforeEach(() => {
    dispatch = jest.fn();
    mockApiHelper = new MockApiHelperChangeName();
  });

  test('Dispatch count', async () => {
    await changeChannelNameFactory(mockApiHelper)('id', 'Channel2')(dispatch, mockGetState);
    expect(dispatch.mock.calls.length).toBe(2);
  });

  test('Dispatch types', async () => {
    await changeChannelNameFactory(mockApiHelper)('id', 'Channel2')(dispatch, mockGetState);
    expect(dispatch.mock.calls[0][0].type).toBe(CHAT_APP_CHANNEL_UPDATE_STARTED);
    expect(dispatch.mock.calls[1][0].type).toBe(CHAT_APP_CHANNEL_UPDATE_SUCCESS);
  });

  test('Payload type', async () => {
    await changeChannelNameFactory(mockApiHelper)('id', 'Channel2')(dispatch, mockGetState);
    expect(dispatch.mock.calls[1][0].payload).toHaveProperty('channel');
  });

  test('Called api method', async () => {
    await changeChannelNameFactory(mockApiHelper)('id', 'Channel2')(dispatch, mockGetState);
    expect(mockApiHelper.put.mock.calls.length).toBe(1);
  });

  test('Parameters of called api method', async () => {
    await changeChannelNameFactory(mockApiHelper)('id', 'Channel2')(dispatch, mockGetState);
    expect(mockApiHelper.put.mock.calls[0][1].name).toBe('Channel2');
    expect(mockApiHelper.put.mock.calls[0][1].id).toBe('id');
    expect(mockApiHelper.put.mock.calls[0][1].customData.users).toContain('mail@mail.com');
  });
});

describe('inviteMember', () => {
  let dispatch = jest.fn();
  let mockApiHelper: MockApiHelperInviteMember;

  beforeEach(() => {
    dispatch = jest.fn();
    mockApiHelper = new MockApiHelperInviteMember();
  });

  test('Dispatch count', async () => {
    await inviteMemberToChannelFactory(mockApiHelper)('id', 'someone@else.com')(dispatch, mockGetState);
    expect(dispatch.mock.calls.length).toBe(2);
  });

  test('Dispatch types', async () => {
    await inviteMemberToChannelFactory(mockApiHelper)('id', 'someone@else.com')(dispatch, mockGetState);
    expect(dispatch.mock.calls[0][0].type).toBe(CHAT_APP_CHANNEL_UPDATE_STARTED);
    expect(dispatch.mock.calls[1][0].type).toBe(CHAT_APP_CHANNEL_UPDATE_SUCCESS);
  });

  test('Payload type', async () => {
    await inviteMemberToChannelFactory(mockApiHelper)('id', 'someone@else.com')(dispatch, mockGetState);
    expect(dispatch.mock.calls[1][0].payload).toHaveProperty('channel');
  });

  test('Called api method', async () => {
    await inviteMemberToChannelFactory(mockApiHelper)('id', 'someone@else.com')(dispatch, mockGetState);
    expect(mockApiHelper.put.mock.calls.length).toBe(1);
  });

  test('Parameters of called api method', async () => {
    await inviteMemberToChannelFactory(mockApiHelper)('id', 'someone@else.com')(dispatch, mockGetState);
    expect(mockApiHelper.put.mock.calls[0][1].name).toBe('Channel');
    expect(mockApiHelper.put.mock.calls[0][1].id).toBe('id');
    expect(mockApiHelper.put.mock.calls[0][1].customData.users).toContain('mail@mail.com');
    expect(mockApiHelper.put.mock.calls[0][1].customData.users).toContain('someone@else.com');
  });
});
