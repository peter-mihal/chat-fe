import { updateUserFactory } from '../../../src/chatApp/actions/updateUserProfile';
import {
  CHAT_APP_USER_PROFILE_UPDATE_STARTED,
  CHAT_APP_USER_PROFILE_UPDATE_SUCCESS
} from '../../../src/chatApp/constants/actionTypes';

class MockApiHelper {
  public get = jest.fn();
  public post = jest.fn();
  public put = jest.fn();
  public delete = jest.fn();
}

const mockGetState = () => {
  return {
    chatApp: {
      user: {
        email: 'mail@mail.com',
        username: 'adam',
        isLoggedIn: true,
        customData: {
          username: 'adam',
          photoUri: undefined
        }
      }
    }
  };
};

const userData = {
  username: 'eva',
  photoUri: ''
};

const userData2 = {
  username: 'enoch',
  photoUri: ''
};

const file = {
  createdBy: 'mail@mail.com',
  extension: 'jpg',
  fileSize: 100,
  id: 'img',
  name: 'picture',
  fileUri: 'u/r/i'
};

describe('updateUser', () => {
  let dispatch = jest.fn();
  let mockApiHelper: MockApiHelper;

  beforeEach(() => {
    dispatch = jest.fn();
    mockApiHelper = new MockApiHelper();
  });

  test('Dispatch count', async () => {
    await updateUserFactory(mockApiHelper)(userData)(dispatch, mockGetState);
    expect(dispatch.mock.calls.length).toBe(2);
  });

  test('Dispatch types', async () => {
    await updateUserFactory(mockApiHelper)(userData)(dispatch, mockGetState);
    expect(dispatch.mock.calls[0][0].type).toBe(CHAT_APP_USER_PROFILE_UPDATE_STARTED);
    expect(dispatch.mock.calls[1][0].type).toBe(CHAT_APP_USER_PROFILE_UPDATE_SUCCESS);
  });

  test('Payload type', async () => {
    await updateUserFactory(mockApiHelper)(userData)(dispatch, mockGetState);
    expect(dispatch.mock.calls[1][0].payload).toHaveProperty('user');
  });

  test('Called api method', async () => {
    await updateUserFactory(mockApiHelper)(userData)(dispatch, mockGetState);
    expect(mockApiHelper.put.mock.calls.length).toBe(1);
  });

  test('Parameters of called api method', async () => {
    await updateUserFactory(mockApiHelper)(userData)(dispatch, mockGetState);
    expect(mockApiHelper.put.mock.calls[0][1].customData.username).toBe('eva');
  });

  test('Parameters of called api method - with file', async () => {
    await updateUserFactory(mockApiHelper)(userData2, file)(dispatch, mockGetState);
    expect(mockApiHelper.put.mock.calls[0][1].customData.username).toBe('enoch');
    expect(mockApiHelper.put.mock.calls[0][1].customData.photoUri).toBe('u/r/i');
  });
});
