import {
  loginActionFactory,
  registerActionFactory,
  logoutUser, getUserFactory
} from '../../../src/chatApp/actions/authUser';
import {
  CHAT_APP_GET_USER_SUCCESS,
  CHAT_APP_LOGIN_STARTED,
  CHAT_APP_LOGIN_SUCCESS, CHAT_APP_LOGOUT_STARTED, CHAT_APP_LOGOUT_SUCCESS
} from '../../../src/chatApp/constants/actionTypes';

class MockApiHelperLogin {
  public get = jest.fn(_ => ({
    email: 'mail@mail.com',
    customData: {}
  }));
  public post = jest.fn(_ => ({
    token: 'abc',
    expiration: new Date().toJSON()
  }));
  public put = jest.fn();
  public delete = jest.fn();
}

class MockApiHelperRegistration {
  public get = jest.fn();
  public post = jest.fn(_ => ({
    email: 'mail@mail.com',
    customData: {}
  }));
  public put = jest.fn();
  public delete = jest.fn();
}

class MockApiHelperGetUser {
  public get = jest.fn(_ => ({
    email: 'mail@mail.com',
    customData: {}
  }));
  public post = jest.fn();
  public put = jest.fn();
  public delete = jest.fn();
}

describe('login', () => {
  let mockApiHelper: MockApiHelperLogin;
  let dispatch = jest.fn();

  beforeEach(() => {
    dispatch = jest.fn();
    mockApiHelper = new MockApiHelperLogin();
  });

  test('Dispatch count', async () => {
    await loginActionFactory(mockApiHelper)('mail@mail.com')(dispatch);
    expect(dispatch.mock.calls.length).toBe(2);
  });

  test('Dispatch types', async () => {
    await loginActionFactory(mockApiHelper)('mail@mail.com')(dispatch);
    expect(dispatch.mock.calls[0][0].type).toBe(CHAT_APP_LOGIN_STARTED);
    expect(dispatch.mock.calls[1][0].type).toBe(CHAT_APP_LOGIN_SUCCESS);
  });

  test('Payload type', async () => {
    await loginActionFactory(mockApiHelper)('mail@mail.com')(dispatch);
    expect(dispatch.mock.calls[1][0].payload).toHaveProperty('user');
  });

  test('Called api method', async () => {
    await loginActionFactory(mockApiHelper)('mail@mail.com')(dispatch);
    expect(mockApiHelper.post.mock.calls.length).toBeGreaterThanOrEqual(1);
  });

  test('Parameters of called api method', async () => {
    await loginActionFactory(mockApiHelper)('mail@mail.com')(dispatch);
    expect(mockApiHelper.post.mock.calls[0][1].email).toBe('mail@mail.com');
  });
});

describe('register', () => {
  let mockApiHelper: MockApiHelperRegistration;
  let dispatch = jest.fn();

  beforeEach(() => {
    dispatch = jest.fn();
    mockApiHelper = new MockApiHelperRegistration();
  });

  test('Called api method', async () => {
    await registerActionFactory(mockApiHelper)('mail@mail.com')(dispatch);
    expect(mockApiHelper.post.mock.calls.length).toBeGreaterThanOrEqual(1);
  });

  test('Parameters of called api method', async () => {
    await registerActionFactory(mockApiHelper)('mail@mail.com')(dispatch);
    expect(mockApiHelper.post.mock.calls[0][1].email).toBe('mail@mail.com');
  });
});

describe('logout', () => {
  let dispatch = jest.fn();

  beforeEach(() => {
    dispatch = jest.fn();
  });

  test('Dispatch count', async () => {
    await logoutUser()(dispatch);
    expect(dispatch.mock.calls.length).toBe(2);
  });

  test('Dispatch types', async () => {
    await logoutUser()(dispatch);
    expect(dispatch.mock.calls[0][0].type).toBe(CHAT_APP_LOGOUT_STARTED);
    expect(dispatch.mock.calls[1][0].type).toBe(CHAT_APP_LOGOUT_SUCCESS);
  });

  test('Payload type', async () => {
    await logoutUser()(dispatch);
    expect(dispatch.mock.calls[1][0].payload).toHaveProperty('user');
  });
});

describe('getUser', () => {
  let mockApiHelper: MockApiHelperGetUser;
  let dispatch = jest.fn();

  beforeEach(() => {
    dispatch = jest.fn();
    mockApiHelper = new MockApiHelperGetUser();
  });

  test('Dispatch count', async () => {
    await getUserFactory(mockApiHelper)('mail@mail.com')(dispatch);
    expect(dispatch.mock.calls.length).toBe(1);
  });

  test('Dispatch types', async () => {
    await getUserFactory(mockApiHelper)('mail@mail.com')(dispatch);
    expect(dispatch.mock.calls[0][0].type).toBe(CHAT_APP_GET_USER_SUCCESS);
  });

  test('Payload type', async () => {
    await getUserFactory(mockApiHelper)('mail@mail.com')(dispatch);
    expect(dispatch.mock.calls[0][0].payload).toHaveProperty('user');
  });

  test('Called api method', async () => {
    await getUserFactory(mockApiHelper)('mail@mail.com')(dispatch);
    expect(mockApiHelper.get.mock.calls.length).toBeGreaterThanOrEqual(1);
  });

  test('Parameters of dispatched user', async () => {
    await getUserFactory(mockApiHelper)('mail@mail.com')(dispatch);
    expect(dispatch.mock.calls[0][0].payload.user.email).toBe('mail@mail.com');
  });
});
