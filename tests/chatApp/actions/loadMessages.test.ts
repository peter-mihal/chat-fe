import { loadMessagesFactory } from '../../../src/chatApp/actions/loadMessages';
import {
  CHAT_APP_MESSAGES_LOADING_STARTED,
  CHAT_APP_MESSAGES_LOADING_SUCCESS
} from '../../../src/chatApp/constants/actionTypes';

class MockApiHelper {
  public get = jest.fn(_ => ([]));
  public post = jest.fn();
  public put = jest.fn();
  public delete = jest.fn();
}

describe('loadMessage', () => {
  let dispatch = jest.fn();
  let mockApiHelper: MockApiHelper;

  beforeEach(() => {
    dispatch = jest.fn();
    mockApiHelper = new MockApiHelper();
  });

  test('Dispatch count', async () => {
    await loadMessagesFactory(mockApiHelper)('mail@mail.com')(dispatch);
    expect(dispatch.mock.calls.length).toBe(2);
  });

  test('Dispatch types', async () => {
    await loadMessagesFactory(mockApiHelper)('mail@mail.com')(dispatch);
    expect(dispatch.mock.calls[0][0].type).toBe(CHAT_APP_MESSAGES_LOADING_STARTED);
    expect(dispatch.mock.calls[1][0].type).toBe(CHAT_APP_MESSAGES_LOADING_SUCCESS);
  });

  test('Payload type', async () => {
    await loadMessagesFactory(mockApiHelper)('mail@mail.com')(dispatch);
    expect(dispatch.mock.calls[1][0].payload).toHaveProperty('messages');
  });

  test('Called api method', async () => {
    await loadMessagesFactory(mockApiHelper)('mail@mail.com')(dispatch);
    expect(mockApiHelper.get.mock.calls.length).toBeGreaterThanOrEqual(1);
  });
});
