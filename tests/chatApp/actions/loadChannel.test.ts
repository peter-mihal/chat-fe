import { loadChannelsFactory } from '../../../src/chatApp/actions/loadChannels';
import {
  CHAT_APP_CHANNELS_LOADING_STARTED,
  CHAT_APP_CHANNELS_LOADING_SUCCESS
} from '../../../src/chatApp/constants/actionTypes';

class MockApiHelper {
  public get = jest.fn(_ => ([{
      id: 'id',
      name: 'Channel',
      customData: {
        users: ['mail@mail.com']
      }
    }, {
      id: 'id2',
      name: 'Channel2',
      customData: {
        users: ['someone@else.com']
      }
    }, {
      id: 'id3',
      name: 'Channel3',
      customData: {
        users: ['someone@else.com', 'mail@mail.com']
      }
    }
  ]));
  public post = jest.fn();
  public put = jest.fn();
  public delete = jest.fn();
}

describe('loadChannel', () => {
  let dispatch = jest.fn();
  let mockApiHelper: MockApiHelper;

  beforeEach(() => {
    dispatch = jest.fn();
    mockApiHelper = new MockApiHelper();
  });

  test('Dispatch count', async () => {
    await loadChannelsFactory(mockApiHelper)('mail@mail.com')(dispatch);
    expect(dispatch.mock.calls.length).toBe(2);
  });

  test('Dispatch types', async () => {
    await loadChannelsFactory(mockApiHelper)('mail@mail.com')(dispatch);
    expect(dispatch.mock.calls[0][0].type).toBe(CHAT_APP_CHANNELS_LOADING_STARTED);
    expect(dispatch.mock.calls[1][0].type).toBe(CHAT_APP_CHANNELS_LOADING_SUCCESS);
  });

  test('Payload type', async () => {
    await loadChannelsFactory(mockApiHelper)('mail@mail.com')(dispatch);
    expect(dispatch.mock.calls[1][0].payload).toHaveProperty('channels');
  });

  test('Called api method', async () => {
    await loadChannelsFactory(mockApiHelper)('mail@mail.com')(dispatch);
    expect(mockApiHelper.get.mock.calls.length).toBeGreaterThanOrEqual(1);
  });

  test('Filtering channels for user', async () => {
    await loadChannelsFactory(mockApiHelper)('mail@mail.com')(dispatch);
    expect(dispatch.mock.calls[1][0].payload.channels).toHaveLength(2);
    expect(dispatch.mock.calls[1][0].payload.channels).toContainEqual({
      id: 'id',
      name: 'Channel',
      customData: {
        users: ['mail@mail.com']
      }
    });
    expect(dispatch.mock.calls[1][0].payload.channels).toContainEqual({
      id: 'id3',
      name: 'Channel3',
      customData: {
        users: ['someone@else.com', 'mail@mail.com']
      }
    });
  });
});
