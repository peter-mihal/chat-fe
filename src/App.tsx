import * as React from 'react';
import { Provider } from 'react-redux';
import {
  BrowserRouter,
  Route
} from 'react-router-dom';
import './common/common.less';
import { ChatApp } from './chatApp/components/ChatApp';
import { createStore } from './chatApp/utils/createStore';

export class App extends React.PureComponent {
  render() {
    return (
      <Provider store={createStore()}>
        <BrowserRouter>
          <Route render={() => (
            <ChatApp/>
          )}/>
        </BrowserRouter>
      </Provider>
    );
  }
}
