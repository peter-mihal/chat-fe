import {Action as ReduxAction, Store as ReduxStore} from 'redux';
import {MapDispatchToProps as MapReduxDispatchToReactProps, MapStateToProps as MapReduxStateToReactProps} from 'react-redux';
import { IChatApp } from '../chatApp/models/IChatApp';

export type Action = ReduxAction<string> & { payload: { [anything: string]: unknown } };

export type Store = ReduxStore<IChatApp, Action>;

export type MapStateToProps<TStateProps, TOwnProps = any> = MapReduxStateToReactProps<TStateProps, TOwnProps, IChatApp>;

export type MapDispatchToProps<TDispatchProps, TOwnProps = any> = MapReduxDispatchToReactProps<TDispatchProps, TOwnProps>;

