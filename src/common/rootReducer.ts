import { IState } from './IState';
import { chatApp } from '../chatApp/reducers/chatApp';

export const rootReducer = (prevState = {} as IState, action: Action): IState => ({
  chatApp: chatApp(prevState.chatApp, action),
});
