import { IChatApp } from '../chatApp/models/IChatApp';

export interface IState {
  chatApp: IChatApp;
}
