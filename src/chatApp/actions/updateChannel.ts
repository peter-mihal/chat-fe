import {Dispatch} from 'redux';
import {IState} from '../../common/IState';
import {IChannel} from '../models/IChannel';
import {CHAT_APP_CHANNEL_UPDATE_STARTED, CHAT_APP_CHANNEL_UPDATE_SUCCESS} from '../constants/actionTypes';
import {ApiHelper, IApiHelper} from '../../api/ApiHelper';

const channelUpdateStarted = (): Action => ({
  type: CHAT_APP_CHANNEL_UPDATE_STARTED,
});

const channelUpdateSuccess = (channel: IChannel): Action => ({
  type: CHAT_APP_CHANNEL_UPDATE_SUCCESS,
  payload: {
    channel
  }
});

export const changeChannelNameFactory = (api: IApiHelper) => {
  return (id: Uuid, newName: string): any =>
    async (dispatch: Dispatch, getState: () => IState): Promise<void> => {
      dispatch(channelUpdateStarted());

      const oldChannel = getState().chatApp.channels.byId.get(id);
      if (oldChannel) {
        const newChannel = await api.put('channel/' + id, {...oldChannel, name: newName}, true);

        dispatch(channelUpdateSuccess(newChannel));
      }
    };
  };

export const changeChannelName = changeChannelNameFactory(new ApiHelper());

export const inviteMemberToChannelFactory = (api: IApiHelper) => {
  return (id: Uuid, email: string): any =>
    async (dispatch: Dispatch, getState: () => IState): Promise<void> => {
      dispatch(channelUpdateStarted());

      const oldChannel = getState().chatApp.channels.byId.get(id);
      if (oldChannel) {
        const users = new Set<string>(oldChannel.customData.users);
        const newMembers = users.add(email);
        const newChannel = await api.put('channel/' + id, {...oldChannel, customData: {
            users: newMembers,
          }}, true);

        dispatch(channelUpdateSuccess(newChannel));
      }
    };
  };

export const inviteMemberToChannel = inviteMemberToChannelFactory(new ApiHelper());
