import { Dispatch } from 'redux';
import {ApiHelper, IApiHelper} from '../../api/ApiHelper';
import {
  CHAT_APP_GET_USER_SUCCESS,
  CHAT_APP_LOGIN_FAIL,
  CHAT_APP_LOGIN_STARTED,
  CHAT_APP_LOGIN_SUCCESS,
  CHAT_APP_LOGOUT_STARTED,
  CHAT_APP_LOGOUT_SUCCESS,
  CHAT_APP_REGISTER_FAIL,
  CHAT_APP_REGISTER_STARTED,
  CHAT_APP_REGISTER_SUCCESS
} from '../constants/actionTypes';
import {
  IUser
} from '../models/IChatApp';

const loginStarted = (): Action => ({
  type: CHAT_APP_LOGIN_STARTED,
});

const loginFail = (): Action => ({
  type: CHAT_APP_LOGIN_FAIL,
});

const registerStarted = (): Action => ({
  type: CHAT_APP_REGISTER_STARTED,
});
const registerSuccess = (): Action => ({
  type: CHAT_APP_REGISTER_SUCCESS,
});
const registerFail = (): Action => ({
  type: CHAT_APP_REGISTER_FAIL,
});


const loginSuccess = (user: IUser, users: IUser[]): Action => ({
  type: CHAT_APP_LOGIN_SUCCESS,
  payload: {
    user,
    users
  }
});

const getUserSuccess = (user: IUser): Action => ({
  type: CHAT_APP_GET_USER_SUCCESS,
  payload: {
    user
  }
});

const logoutUserStarted = (): Action => ({
  type: CHAT_APP_LOGOUT_STARTED
});

const logoutUserSuccess = (user: IUser): Action => ({
  type: CHAT_APP_LOGOUT_SUCCESS,
  payload: {
    user
  }
});

export const loginActionFactory = (api: IApiHelper) => {
  return (email: string): any =>
    async (dispatch: Dispatch): Promise<void> => {
      dispatch(loginStarted());
      try {

        const res = await api.post('auth', {email}, false);
        // get an set token
        localStorage.setItem('token', res.token);

        // get user data
        const userApi = await api.get('user/' + email, true);
        const usersApi = await api.get('user/', true);

        const user = {
          email: userApi.email,
          username: '',
          isLoggedIn: true,
          customData: {
            photoUri: userApi.customData ? userApi.customData.photoUri : '',
            username: userApi.customData ? userApi.customData.username : ''
          }
        };

        dispatch(loginSuccess(user, usersApi));
        return res;
      }
      catch (e) {
        dispatch(loginFail());
        throw e;
      }
    };
  };

export const loginAction = loginActionFactory(new ApiHelper());

export const registerActionFactory = (api: IApiHelper) => {
  return (email: string): any =>
    async (dispatch: Dispatch): Promise<void> => {
      dispatch(registerStarted());
      try {
        await api.post('user', {email}, true);
        dispatch(registerSuccess());
      } catch (e) {
        dispatch(registerFail());
        throw e;
      }
    };
  };

export const registerAction = registerActionFactory(new ApiHelper());

export const logoutUser = (): any =>
  async (dispatch: Dispatch): Promise<void> => {
    dispatch(logoutUserStarted());
    const user = {
      email: '',
      username: '',
      isLoggedIn: false,
      customData: {
        username: '',
        photoUri: ''
      }
    };

    localStorage.setItem('token', '');
    localStorage.removeItem('token');
    dispatch(logoutUserSuccess(user));
  };

export const getUserFactory = (api: IApiHelper) => {
  return (email: string): any =>
    async (dispatch: Dispatch): Promise<void> => {
      // alert('get user');
      const getUserApi = await api.get('user/' + email, true);
      const user = {
        email,
        username: email.split('@')[0],
        isLoggedIn: true,
        customData: getUserApi.customData ? getUserApi.customData : {
          username: email,
          photoUri: ''
        }
      };
      dispatch(getUserSuccess(user));
    };
  };

export const getUser = getUserFactory(new ApiHelper());
