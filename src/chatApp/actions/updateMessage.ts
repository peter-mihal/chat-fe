import { Dispatch } from 'redux';
import {ApiHelper, IApiHelper} from '../../api/ApiHelper';
import { IState } from '../../common/IState';
import {
  CHAT_APP_MESSAGE_UPDATE_STARTED,
  CHAT_APP_MESSAGE_UPDATE_SUCCESS,
} from '../constants/actionTypes';
import { IMessage } from '../models/IMessage';

const messageUpdateStarted = (): Action => ({
  type: CHAT_APP_MESSAGE_UPDATE_STARTED,
});

const messageUpdateSuccess = (message: IMessage): Action => ({
  type: CHAT_APP_MESSAGE_UPDATE_SUCCESS,
  payload: {
    message
  }
});

export const dislikeMessageFactory = (api: IApiHelper) => {
  return (id: Uuid): any =>
    async (dispatch: Dispatch, getState: () => IState): Promise<void> => {
      dispatch(messageUpdateStarted());

      try {
        const oldMessage = getState().chatApp.messages.byId.get(id);
        if (oldMessage) {
          // @ts-ignore
          const message: IMessage = {
            ...oldMessage,
            customData: {
              ...oldMessage.customData,
              dislikes: oldMessage.customData.dislikes + 1
            }
          };
          const messageApi = await api.put('channel/' + message.customData.channelId + '/message/' + message.id, message, true);

          dispatch(messageUpdateSuccess(messageApi));
        }
      } catch (e) {
        throw e;
      }
    };
  };

export const dislikeMessage = dislikeMessageFactory(new ApiHelper());

export const likeMessageFactory = (api: IApiHelper) => {
  return (id: Uuid): any =>
    async (dispatch: Dispatch, getState: () => IState): Promise<void> => {
      dispatch(messageUpdateStarted());

      try {
        const oldMessage = getState().chatApp.messages.byId.get(id);
        if (oldMessage) {
          // @ts-ignore
          const message: IMessage = {
            ...oldMessage,
            customData: {
              ...oldMessage.customData,
              likes: oldMessage.customData.likes + 1
            }
          };
          const messageApi = await api.put('channel/' + message.customData.channelId + '/message/' + message.id, message, true);

          dispatch(messageUpdateSuccess(messageApi));
        }
      } catch (e) {
        throw e;
      }
    };
  };

export const likeMessage = likeMessageFactory(new ApiHelper());
