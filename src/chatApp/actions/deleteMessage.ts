import { Dispatch } from 'redux';
import {ApiHelper, IApiHelper} from '../../api/ApiHelper';
import { IState } from '../../common/IState';
import {
  CHAT_APP_MESSAGE_DELETE_COMPLETE_STARTED,
  CHAT_APP_MESSAGE_DELETE_COMPLETE_SUCCESS
} from '../constants/actionTypes';
import { IMessage } from '../models/IMessage';

const deleteMessageStarted = (): Action => ({
  type: CHAT_APP_MESSAGE_DELETE_COMPLETE_STARTED,
});

const deleteMessageSuccess = (messages: IMessage[]): Action => ({
  type: CHAT_APP_MESSAGE_DELETE_COMPLETE_SUCCESS,
  payload: {
    messages
  }
});

export const deleteMessageFactory = (api: IApiHelper) => {
  return (id: Uuid): any =>
    async (dispatch: Dispatch, getState: () => IState): Promise<void> => {
      dispatch(deleteMessageStarted());

      try {
        // @ts-ignore
        const message: IMessage = getState().chatApp.messages.byId.get(id);
        await api.delete('channel/' + message.customData.channelId + '/message/' + message.id, true);
        const messages = await api.get('channel/' + message.customData.channelId + '/message', true);
        dispatch(deleteMessageSuccess(messages));
      }
      catch (e) {
        throw e;
      }
    };
  };

export const deleteMessage = deleteMessageFactory(new ApiHelper());
