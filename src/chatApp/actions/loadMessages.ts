import { Dispatch } from 'redux';
import {ApiHelper, IApiHelper} from '../../api/ApiHelper';
import {
  CHAT_APP_MESSAGES_LOADING_STARTED,
  CHAT_APP_MESSAGES_LOADING_SUCCESS
} from '../constants/actionTypes';
import { IMessage } from '../models/IMessage';

const loadingStarted = (): Action => ({
  type: CHAT_APP_MESSAGES_LOADING_STARTED,
});

const loadingSuccess = (messages: ReadonlyArray<IMessage>): Action => ({
  type: CHAT_APP_MESSAGES_LOADING_SUCCESS,
  payload: {
    messages,
  }
});

export const loadMessagesFactory = (api: IApiHelper) => {
  return (id: string): any =>
    async (dispatch: Dispatch): Promise<void> => {
      try {
        dispatch(loadingStarted());
        const messages = await api.get('channel/' + id + '/message', true);
        dispatch(loadingSuccess(messages));
      } catch (e) {
        // error
      }
    };
  };

export const loadMessages = loadMessagesFactory(new ApiHelper());
