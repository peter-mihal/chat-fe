import { Dispatch } from 'redux';
import {
  CHAT_APP_CHANNEL_DELETE_STARTED,
  CHAT_APP_CHANNEL_DELETE_SUCCESS
} from '../constants/actionTypes';
import { IChannel } from '../models/IChannel';
import {ApiHelper, IApiHelper} from '../../api/ApiHelper';

const deleteChannelStarted = (): Action => ({
  type: CHAT_APP_CHANNEL_DELETE_STARTED,
});

const deleteChannelSuccess = (remainingChannels: ReadonlyArray<IChannel>): Action => ({
  type: CHAT_APP_CHANNEL_DELETE_SUCCESS,
  payload: {
    channels: remainingChannels,
  }
});

export const deleteChannelFactory = (api: IApiHelper) => {
  return (id: Uuid, userEmail: string): any =>
    async (dispatch: Dispatch): Promise<void> => {
      dispatch(deleteChannelStarted());
      await api.delete('channel/' + id, true);
      const allChannels = await api.get('channel', true);
      const channels = allChannels.filter((channel: IChannel) => {
        const users = new Set<string>(channel.customData.users);
        return users.has(userEmail);
      });
      dispatch(deleteChannelSuccess(channels));
    };
  };

export const deleteChannel = deleteChannelFactory(new ApiHelper());
