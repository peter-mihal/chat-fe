import {
  CHAT_APP_MESSAGES_FILTER,
  CHAT_APP_CHANNEL_CHANGE_ORDER
} from '../constants/actionTypes';

export const changeChannelOrder = (index: number, direction: Direction): Action => ({
  type: CHAT_APP_CHANNEL_CHANGE_ORDER,
  payload: {
    index,
    direction,
  }
});

export const setMessagesFilter = (filter: string): Action => ({
  type: CHAT_APP_MESSAGES_FILTER,
  payload: {
    filter,
  }
});
