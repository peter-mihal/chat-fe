import { Dispatch } from 'redux';
import {ApiHelper, IApiHelper} from '../../api/ApiHelper';

import {
  CHAT_APP_FILE_CREATE_SUCCESS,
  CHAT_APP_MESSAGE_CREATE_STARTED,
  CHAT_APP_MESSAGE_CREATE_SUCCESS
} from '../constants/actionTypes';
import { IUser } from '../models/IChatApp';
import {
  IFile,
  IMessage
} from '../models/IMessage';

const createMessageStarted = (): Action => ({
  type: CHAT_APP_MESSAGE_CREATE_STARTED,
});

const createFileSuccess = (): Action => ({
  type: CHAT_APP_FILE_CREATE_SUCCESS
});

const createMessageSuccess = (message: IMessage): Action => ({
  type: CHAT_APP_MESSAGE_CREATE_SUCCESS,
  payload: {
    message,
  }
});

export const createFileActionFactory = (api: IApiHelper) => {
  return (file: any): any =>
    async (dispatch: Dispatch): Promise<IFile> => {
      try {
        const formData = new FormData();
        formData.append('Files', file);
        const fileRes = await api.post('file/', formData, false, true);

        // get link for added file - previous API for adding does not return link
        const fileUri = await api.get('file/' + fileRes[0].id + '/download-link', false);
        dispatch(createFileSuccess());

        const addedFile: IFile = fileRes[0];
        addedFile.fileUri = fileUri.fileUri;
        return addedFile;
      }
      catch (e) {
        throw e;
      }
    };
  };

export const createFileAction = createFileActionFactory(new ApiHelper());

export const createMessageActionFactory = (api: IApiHelper) => {
  return (text: string, user: IUser, channelId: string, file?: IFile): any =>
    async (dispatch: Dispatch): Promise<void> => {
      dispatch(createMessageStarted());

      try {
        const message = {
          id: '',
          value: text,
          createdBy: user.email,
          createdAt: new Date().toISOString(),
          updatedAt: new Date().toISOString(),
          updatedBy: user.email,
          customData: {
            likes: 0,
            dislikes: 0,
            file,
            channelId,
            userPhoto: user.customData.photoUri
          }
        };

        const messageApi = await api.post('channel/' + channelId + '/message', message, true);

        dispatch(createMessageSuccess(messageApi));
        return messageApi;
      }
      catch (e) {
        throw e;
      }

    };
  };

export const createMessageAction = createMessageActionFactory(new ApiHelper());
