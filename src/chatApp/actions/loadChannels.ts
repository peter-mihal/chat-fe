import {Dispatch} from 'redux';
import {CHAT_APP_CHANNELS_LOADING_STARTED, CHAT_APP_CHANNELS_LOADING_SUCCESS} from '../constants/actionTypes';
import {IChannel} from '../models/IChannel';
import {ApiHelper, IApiHelper} from '../../api/ApiHelper';

const loadingStarted = (): Action => ({
  type: CHAT_APP_CHANNELS_LOADING_STARTED,
});

const loadingSuccess = (channels: ReadonlyArray<IChannel>): Action => ({
  type: CHAT_APP_CHANNELS_LOADING_SUCCESS,
  payload: {
    channels,
  }
});

export const loadChannelsFactory = (api: IApiHelper) => {
  return (email: string): any =>
    async (dispatch: Dispatch): Promise<void> => {

      dispatch(loadingStarted());
      const allChannels = await api.get('channel', true);
      const channels = allChannels.filter((channel: IChannel) => {
        const users = new Set<string>(channel.customData.users);
        return users.has(email);
      });
      dispatch(loadingSuccess(channels));
    };
  };

export const loadChannels = loadChannelsFactory(new ApiHelper());
