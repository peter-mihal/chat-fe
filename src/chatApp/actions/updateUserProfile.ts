import { Dispatch } from 'redux';
import { ApiHelper, IApiHelper } from '../../api/ApiHelper';
import { IState } from '../../common/IState';
import {
  IUser,
  IUserData
} from '../models/IChatApp';
import {
  CHAT_APP_USER_PROFILE_UPDATE_STARTED,
  CHAT_APP_USER_PROFILE_UPDATE_SUCCESS
} from '../constants/actionTypes';
import { IFile } from '../models/IMessage';

const userProfileUpdateStarted = (): Action => ({
  type: CHAT_APP_USER_PROFILE_UPDATE_STARTED,
});

const userProfileUpdateSuccess = (user: IUser): Action => ({
  type: CHAT_APP_USER_PROFILE_UPDATE_SUCCESS,
  payload: {
    user
  }
});

export const updateUserFactory = (api: IApiHelper) => {
  return (data: IUserData, file?: IFile): any =>
    async (dispatch: Dispatch, getState: () => IState): Promise<void> => {
      dispatch(userProfileUpdateStarted());
      try {
        const oldUser = getState().chatApp.user;
        if (oldUser) {
          const customData = {
            ...oldUser.customData,
            username: data.username ? data.username : oldUser.customData.username,
            photoUri: file ? file.fileUri : oldUser.customData.photoUri
          };

          const userApi = await api.put('user/' + oldUser.email, {customData}, true);
          const newUser = (Object.assign({}, oldUser.customData, {...userApi}));
          dispatch(userProfileUpdateSuccess(newUser));
        }
      }
      catch (e) {
        throw e;
      }
    };
  };

export const updateUser = updateUserFactory(new ApiHelper());
