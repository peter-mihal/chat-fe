import * as Immutable from 'immutable';
import {Dispatch} from 'redux';
import {IChannel} from '../models/IChannel';
import {CHAT_APP_CHANNEL_CREATE_STARTED, CHAT_APP_CHANNEL_CREATE_SUCCESS} from '../constants/actionTypes';
import {ApiHelper, IApiHelper} from '../../api/ApiHelper';

const channelCreateStarted = (): Action => ({
  type: CHAT_APP_CHANNEL_CREATE_STARTED,
});

const channelCreateSuccess = (channel: IChannel): Action => ({
  type: CHAT_APP_CHANNEL_CREATE_SUCCESS,
  payload: {
    channel
  }
});

export const createChannelFactory = (api: IApiHelper) => {
  return (name: string, userEmail: string): any =>
    async (dispatch: Dispatch): Promise<void> => {
      dispatch(channelCreateStarted());

      const newChannel = await api.post('channel', {
        name,
        customData: {
          users: Immutable.Set<string>([userEmail])
        }
      }, true);

      dispatch(channelCreateSuccess(newChannel));
    };
  };

export const createChannel = createChannelFactory(new ApiHelper());
