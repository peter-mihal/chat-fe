import * as React from 'react';
import * as Immutable from 'immutable';
import { RouteComponentProps } from 'react-router';
import { MessageContainer } from '../containers/Message';
import { NewMessageContainer } from '../containers/NewMessage';
import { withLocation } from '../utils/withLocation';
import * as ReactLoader from 'react-loader';

export interface IContentStateProps {
  readonly messagesIds: Immutable.List<Uuid>;
  readonly isLoading: boolean;
}

interface ChannelRouteParams {
  readonly id: string;
  readonly name: string;
}

export interface IContentDispatchProps {
  readonly loadMessages: (id: string) => void;
  readonly filterMessages: (searchText: string) => void;
}


type ContentProps = IContentDispatchProps & IContentStateProps & RouteComponentProps<ChannelRouteParams>;

class Content extends React.PureComponent<ContentProps> {
  private messagesEnd: any;
  private interval: any;

  constructor(props: ContentProps) {
    super(props);
  }

  componentDidMount() {
    this.getMessages(this.props.match.params.id);

    // Simulate real time app refresh every minute
    this.interval = setInterval(() => {

      this.getMessages(this.props.match.params.id);
    }, 20000);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  componentWillReceiveProps(nextProps: Readonly<ContentProps>) {
    if (nextProps.match.params.id !== this.props.match.params.id) {
      this.getMessages(nextProps.match.params.id);
    }
  }

  scrollToBottom = () => {
    this.messagesEnd.scrollIntoView({behavior: 'instant'});
  };

  componentDidUpdate() {
    this.scrollToBottom();
  }

  private getMessages(channelId: string) {
    this.props.loadMessages(channelId);
    this.scrollToBottom();
  }

  private onSearchChanged = (event: React.FormEvent<HTMLInputElement>) => {
    const searchText = event.currentTarget.value;
    this.props.filterMessages(searchText);
  };

  render() {
    return (
      <div className="col-lg-8 col-sm-12 content d-flex flex-column justify-content-between">
        <div className="row h-10 content-header content-header-min-height">
          <div className="col-12">
            <div className="d-flex h-100 align-items-center justify-content-between">
              <span>
                {this.props.location.state.title}
              </span>

              <div className="input-group w-25">
                <input type="text"
                       className="form-control"
                       placeholder="Search"
                       autoComplete="off"
                       onChange={this.onSearchChanged}/>

                <div className="input-group-append">
                  <span className="input-group-text background-white">
                    <i className="fa fa-search"/>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="row h-70">
          <div className="messages h-100 container-fluid">
            {
              this.props.messagesIds.map((id: Uuid) => (
                <MessageContainer id={id} key={id}/>
              ))
            }
            <ReactLoader loaded={!this.props.isLoading}/>

            <div
              style={{
                float: 'left',
                clear: 'both'
              }}
              ref={
                (el) => {
                  this.messagesEnd = el;
                }}
            />
          </div>
        </div>

        <NewMessageContainer channelId={this.props.match.params.id}/>
      </div>
    );
  }
}

const ContentWithLocation = withLocation(Content);

export { ContentWithLocation as Content };
