import * as React from 'react';
import * as sanitizeHtml from 'sanitize-html';
import { IMessage } from '../models/IMessage';

export interface IMessageOwnProps {
  readonly id: Uuid;
  readonly index: number;
}

export interface IMessageStateProps {
  readonly message: IMessage;
}

export interface IMessageDispatchProps {
  readonly onDelete: () => void;
  readonly like: () => void;
  readonly dislike: () => void;
}

type MessageProps = IMessageOwnProps & IMessageStateProps & IMessageDispatchProps;

export class Message extends React.PureComponent<MessageProps> {
  render() {
    const {index, message} = this.props;

    const allowedHtmlTags = {
      allowedTags: ['b', 'span', 'i', 'em', 'strong', 'a', 'p', 'h1', 'h2', 'h3', 'h4', 'h5', 'ul', 'ol', 'li'],
      allowedAttributes: {
        a: ['href', 'target'],
        strong: ['style'],
        i: ['class'],
        span: ['class', 'data-index', 'data-denotation-char', 'data-id', 'data-value', 'contenteditable']
      }
    };


    return (
      <div className="row message-row" key={index}>
        <div className="col-10 d-flex flex-column">
          <div className="message-content d-flex mt-2 align-items-center">
            <img src={message.customData.userPhoto ? message.customData.userPhoto : '/default-user.jpg'}
                 className="profile-pic"/>
            <p className="m-2"
               dangerouslySetInnerHTML={{__html: sanitizeHtml(message.value, allowedHtmlTags)}}/>
          </div>

          <img src={message.customData.file ? message.customData.file.fileUri : ''}
               className="img-radius mw-30"/>
        </div>

        <div className="col-2 d-flex align-items-center justify-content-between action-buttons">
          <span>
            <i className="fa fa-thumbs-o-up hoverable" onClick={this.props.like}/>
            <sup>{message.customData ? message.customData.likes : ''}</sup>
          </span>

          <span>
            <i className="fa fa-thumbs-o-down hoverable" onClick={this.props.dislike}/>
            <sup>{message.customData ? message.customData.dislikes : ''}</sup>
          </span>

          <i className="fa fa-trash-o hoverable" onClick={this.props.onDelete}/>

        </div>
      </div>
    );
  }
}
