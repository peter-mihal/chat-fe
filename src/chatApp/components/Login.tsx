import * as React from 'react';
import {
  RouteComponentProps
} from 'react-router';
import {
  toast
} from 'react-toastify';
import { IUser } from '../models/IChatApp';
import * as ReactLoader from 'react-loader';

export interface ILoginStateProps {
  readonly user: IUser;
  readonly isLoading: boolean;
}

export interface ILoginDispatchProps {
  readonly onLogin: (data: any) => Promise<any>;
  readonly onRegister: (data: any) => Promise<any>;
}

interface ILoginState {
  readonly value: string;
  readonly isValid: boolean;
  readonly isTouched: boolean;
}

const emailRegexp = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
type loginProps = ILoginStateProps & ILoginDispatchProps & RouteComponentProps<any>;

export class Login extends React.PureComponent<loginProps, ILoginState> {
  constructor(props: loginProps) {
    super(props);

    this.state = {
      value: '',
      isValid: false,
      isTouched: false
    };
  }

  public login = (event: React.MouseEvent<HTMLButtonElement>) => {
    event.preventDefault();
    const email = this.state.value;

    this.props.onLogin(email)
      .then(() => {
        toast.success('Welcome ' + this.state.value + '.');
        this.props.history.push('/channels');
      })
      .catch(() => {
        toast.error('Login name does not exists');
      });
  };

  public register = (event: React.MouseEvent<HTMLButtonElement>) => {
    event.preventDefault();

    const email = this.state.value;
    this.props.onRegister(email)
      .then(() => {
        toast.success('Welcome to chat ' + this.state.value + '! Please Continue with login.');
      })
      .catch(() => {
        toast.error('Login name does not exists');
      });
  };

  private onEmailChanged = (event: React.FormEvent<HTMLInputElement>) => {
    const {value} = event.currentTarget;
    const isTouched = true;
    this.setState(() => ({isTouched}));
    this.setState(() => ({value}));
    const isValid = event.currentTarget && emailRegexp.test(event.currentTarget.value);
    this.setState(() => ({isValid}));
  };

  render(): JSX.Element {
    // @ts-ignore
    return (
      <div className="d-flex main-background w-100 h-100 justify-content-center align-items-center">
        <ReactLoader loaded={!this.props.isLoading}/>

        <form className="d-flex flex-column align-items-center p-5">

          <h2 className="main-font p-lg-3">
            <small className="text-muted mr-2">Login</small>
            <span className="text-primary">to chat</span>
          </h2>

          <div className="form-group">
            <div className="input-group p-3">
              <div className="input-group-prepend">
                <span className="input-group-text background-white">
                    <i className="fa fa-user-circle"/>
                </span>
              </div>
              <input type="email"
                     className={'form-control ' + (this.state.isTouched && !this.state.isValid ? 'is-invalid' : '')}
                     required
                     value={this.state.value}
                     onChange={this.onEmailChanged}
                     placeholder="Email"/>
            </div>

            <div className="text-center">
              <small className="text-danger"
                     hidden={!this.state.isTouched || this.state.isValid}>
                Must be an email!
              </small>
            </div>
          </div>


          <div>
            <button
              disabled={!this.state.isValid}
              onClick={this.login}
              className="btn btn-outline-primary mb-2">
              Log in <small>with an existing email</small>
            </button>

          </div>

          <div>
            <button
              disabled={!this.state.isValid}
              onClick={this.register}
              className="btn btn-sm btn-outline-secondary">
              Register <small>with a new email</small>
            </button>
          </div>
        </form>
      </div>
    );
  }
}
