import * as React from 'react';
import {IChannel} from '../models/IChannel';

export interface IChannelEditProps {
  readonly channel: IChannel;
  readonly onEditCanceled: () => void;
  readonly onUpdate: (newName: string) => void;
}

interface IChannelEditState {
  readonly value: string;
}

export class ChannelEdit extends React.PureComponent<IChannelEditProps, IChannelEditState> {

  constructor(props: IChannelEditProps) {
    super(props);

    this.state = {
      value: props.channel.name,
    };
  }

  private onValueChanged = (event: React.FormEvent<HTMLInputElement>) => {
    const {value} = event.currentTarget;
    this.setState(_ => ({ value }));
  };

  render() {
    return (
      <form
        className="d-flex justify-content-between w-100"
        onSubmit={
          () => this.props.onUpdate(this.state.value)
        }>
        <input
          value={this.state.value}
          onChange={this.onValueChanged}
          type="text"
          className="form-control"
          required
          maxLength={64}
        />
        <div className="d-flex justify-content-end">
          <button type="submit" className="btn btn-sm btn-primary ml-2">Save</button>
          <button type="button" className="btn btn-sm btn-default ml-2" onClick={this.props.onEditCanceled}>Cancel</button>
        </div>
      </form>
    );
  }
}
