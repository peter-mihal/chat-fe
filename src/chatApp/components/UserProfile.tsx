import * as React from 'react';
import {
  IUserData
} from '../models/IChatApp';
import { IFile } from '../models/IMessage';

export interface IUserProfileDispatchProps {
  readonly onProfileChanged: (data: IUserData, file?: IFile) => Promise<any>;
  readonly onFileAdd: (file: any) => Promise<any>;
}

export interface IUserState {
  readonly data: IUserData;
}

export interface IUserOwnState {
  readonly data: IUserData;
  readonly file: File | undefined;
  readonly imagePreviewUrl: string;
}

type IProps = IUserProfileDispatchProps & IUserState;

export class UserProfile extends React.PureComponent<IProps, IUserOwnState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      data: {
        username: '',
        photoUri: ''
      },
      file: undefined,
      imagePreviewUrl: ''
    };
  }

  private onSubmit = (event: React.FormEvent) => {
    event.preventDefault();
    if (this.state.file) {
      this.props.onFileAdd(this.state.file)
        .then((addedFile: IFile) => {
          this.props.onProfileChanged(this.state.data, addedFile)
            .then(() => {
              // @ts-ignore
              document.getElementById('hideButton').click();
            })
            .catch(e => {
              console.log(e);
            });
        })
        .catch((err: any) => console.log(err));
    } else {
      this.props.onProfileChanged(this.state.data)
        .then(() => {
          // @ts-ignore
          document.getElementById('hideButton').click();
        })
        .catch(e => {
          console.log(e);
        });
    }

  };

  private onValueChanged = (event: React.FormEvent<HTMLInputElement>) => {
    const data = {...this.state.data};
    data.username = event.currentTarget.value;
    this.setState(_ => ({data}));
  };

  private readImage = (event: React.FormEvent<HTMLInputElement>) => {
    const input = event.currentTarget;

    if (input.files && input.files[0]) {
      const reader = new FileReader();
      const file = input.files[0];

      reader.onload = () => {
        // @ts-ignore
        this.setState(_ => ({
          file,
          imagePreviewUrl: reader.result,
        }));
        // @ts-ignore
        document.getElementsByClassName('imgPreview')[0].setAttribute('style', 'background-image: url(' + this.state.imagePreviewUrl + ')');

      };

      reader.readAsDataURL(input.files[0]);
    }
  };

  private removeImage = () => {
    // @ts-ignore
    this.setState(_ => ({
      file: null,
      imagePreviewUrl: ''
    }));
    document.getElementsByClassName('imgPreview')[0].setAttribute('style', 'background-image: url(' + '' + ')');
  };

  render(): JSX.Element {
    const divPreview = (
      <div className="p-1 background-white d-flex user-thumb">

        <div className="imgPreview h-100">
          <i className="fa text-secondary fa-times-circle" onClick={this.removeImage}/>
        </div>

      </div>
    );

    return (
      <div className="modal fade"
           id="profileModal"
           role="dialog">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">
                Edit profile
              </h5>
              <button type="button"
                      className="close"
                      id="hideButton"
                      data-dismiss="modal">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <div className="modal-body">

              <form onSubmit={this.onSubmit}>
                <div className="form-group">
                  <input type="username"
                         className="form-control"
                         placeholder="Username"
                         value={this.state.data.username}
                         onChange={this.onValueChanged}/>
                </div>

                <div className="custom-file">
                  <input type="file"
                         onChange={this.readImage}
                         className="custom-file-input"
                         id="validatedCustomFile"/>
                  <label className="custom-file-label"
                         htmlFor="validatedCustomFile">
                    {this.state.file ? this.state.file.name : 'Choose profile picture...'}
                  </label>
                </div>

                {this.state.imagePreviewUrl ? divPreview : ''}

                <div className="modal-footer">
                  <button type="button"
                          className="btn btn-secondary"
                          data-dismiss="modal">
                    Close
                  </button>
                  <button type="submit"
                          className="btn btn-primary">
                    Save changes
                  </button>
                </div>

              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
