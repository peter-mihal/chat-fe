import * as React from 'react';

export interface IAddChannelModalProps {
  readonly onSave: (name: string) => void;
}

interface IAddChannelModalState {
  readonly value: string;
}

export class AddChannelModal extends React.PureComponent<IAddChannelModalProps, IAddChannelModalState> {

  constructor(props: IAddChannelModalProps) {
    super(props);

    this.state = {
      value: '',
    };
  }

  private onValueChanged = (event: React.FormEvent<HTMLInputElement>) => {
    const {value} = event.currentTarget;
    this.setState(_ => ({ value }));
  };

  private onSave = (event: React.FormEvent) => {
    event.preventDefault();
    const name = this.state.value;
    const closeBtn = document.getElementById('addChannelModalCloseBtn');
    if (closeBtn) {
      closeBtn.click();
    }
    this.props.onSave(name);
  };

  private onCancel = () => {
    this.setState(_ => ({ value: '' }));
  };

  render(): JSX.Element {
    return (
      <div className="modal fade"
           id="addChannelModal"
           role="dialog">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">
                Add channel
              </h5>
              <button
                type="button"
                id="addChannelModalCloseBtn"
                className="close"
                data-dismiss="modal"
                onClick={() => {
                  this.onCancel();
                }}>
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <form onSubmit={this.onSave}>
              <div className="modal-body">
                <div className="form-group">
                  <input
                    value={this.state.value}
                    onChange={this.onValueChanged}
                    type="text"
                    className="form-control"
                    required
                    maxLength={64}
                    placeholder="Channel name"/>
                </div>
              </div>

              <div className="modal-footer">
                <button type="button"
                        className="btn btn-secondary"
                        data-dismiss="modal"
                        onClick={() => {
                          this.onCancel();
                        }}>
                  Close
                </button>
                <button type="submit"
                        className="btn btn-primary">
                  Create channel
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}
