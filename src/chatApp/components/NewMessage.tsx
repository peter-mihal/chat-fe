import * as React from 'react';
import 'quill-mention';

import ReactQuill from 'react-quill';
import {
  IUser,
  IUserDropdown
} from '../models/IChatApp';
import {
  IFile
} from '../models/IMessage';

export interface INewMessageDispatchProps {
  readonly onMessageAdd: (text: string, user: IUser, channelId: string, file?: IFile) => void;
  readonly onFileAdd: (file: any) => Promise<any>;
}

export interface INewMessagesOwnProps {
  readonly channelId: string;
}

export interface INewMessageStateProps {
  readonly user: IUser;
  readonly users: IUser[];
}

interface IState {
  readonly value: string;
  readonly file: File | undefined;
  readonly imagePreviewUrl: string;
}

type messagesProps = INewMessageStateProps & INewMessageDispatchProps & INewMessagesOwnProps;

export class NewMessage extends React.PureComponent<messagesProps, IState> {
  public editorModules: any;

  constructor(props: messagesProps) {
    super(props);
    this.state = {
      value: '',
      file: undefined,
      imagePreviewUrl: ''
    };

    const usersTmp: IUserDropdown[] = [];
    for (const key in this.props.users) {
      if (this.props.users.hasOwnProperty(key)) {
        if (this.props.users[key].customData && this.props.users[key].customData.username) {
          usersTmp.push({
            id: this.props.users[key].email,
            value: this.props.users[key].customData.username,
            img: this.props.users[key].customData.photoUri ? this.props.users[key].customData.photoUri : '/default-user.jpg'
          });
        }
      }
    }
    this.editorModules = {
      mention: {
        allowedChars: /^[A-Za-z\sÅÄÖåäö]*$/,
        mentionDenotationChars: ['@', '#'],
        dataAttributes: ['id', 'value', 'img'],
        renderItem(item: any) {
          return '<span><img class="profile-pic" src="' + item.img + '" /> ' + '@' + item.value + '</span>';
        },
        source(searchTerm: any, renderList: any) {

          if (searchTerm.length === 0) {
            renderList(usersTmp, searchTerm);
          } else {
            const matches = [];
            for (let i = 0; i < usersTmp.length; i++) {
              if (~usersTmp[i].value && ~usersTmp[i].value.toLowerCase().indexOf(searchTerm.toLowerCase())) {
                matches.push(usersTmp[i]);
              }
            }
            renderList(matches, searchTerm);
          }
        },
      },
    };
  }

  private onSubmit = (event: React.FormEvent) => {
    event.preventDefault();
    if (this.state.file || this.state.value) {
      if (this.state.file) {
        this.props.onFileAdd(this.state.file)
          .then((addedFile: IFile) => {
            this.props.onMessageAdd(this.state.value, this.props.user, this.props.channelId, addedFile);
            this.setState(_ => ({value: ''}));
            this.setState(_ => ({imagePreviewUrl: ''}));
            this.setState(_ => ({file: undefined}));
          })
          .catch((err: any) => console.log(err));
      } else {
        this.props.onMessageAdd(this.state.value, this.props.user, this.props.channelId);
        this.setState(_ => ({value: ''}));
      }
    }
  };

  private handleChange = (quillValue: any) => {
    this.setState(_ => ({value: quillValue}));
  };

  private readImage = (event: React.FormEvent<HTMLInputElement>) => {
    const input = event.currentTarget;

    if (input.files && input.files[0]) {
      const reader = new FileReader();
      const file = input.files[0];
      reader.onload = () => {
        // @ts-ignore
        this.setState(_ => ({
          file,
          imagePreviewUrl: reader.result,
        }));
        // @ts-ignore
        document.getElementsByClassName('imgPreview')[0].setAttribute('style', 'background-image: url(' + this.state.imagePreviewUrl + ')');
      };

      reader.readAsDataURL(input.files[0]);
    }
  };

  private removeImage = () => {
    // @ts-ignore
    this.setState(_ => ({
      file: null,
      imagePreviewUrl: ''
    }));
    document.getElementsByClassName('imgPreview')[0].setAttribute('style', 'background-image: url(' + '' + ')');
  };

  render() {
    const divPreview = (
      <div className="w-100 h-50 p-1 background-white d-flex position-absolute top-50">
        <div className="imgPreview h-100">
          <i className="fa text-secondary fa-times-circle" onClick={this.removeImage}/>
        </div>

      </div>
    );
    return (
      <div className="row h-20 position-relative new-message-min-height">

        {this.state.imagePreviewUrl ? divPreview : ''}

        <form onSubmit={this.onSubmit} className="form-inline background-white mt-auto w-100 h-100">

          <div className="col-10 p-0 h-100 w-80">

            <ReactQuill value={this.state.value}
                        theme="snow"
                        modules={this.editorModules}
                        onChange={this.handleChange}/>
          </div>

          <div
            className="col-2 w-20 h-100 p-0 d-flex align-items-center justify-content-around">

            <span className="text-secondary hoverable">
              <input type="file"
                     id="customFile"
                     onChange={this.readImage}/>
              <label htmlFor="customFile">
                <i className="fa fa-paperclip"/>
              </label>
            </span>

            <button type="submit"
                    disabled={this.state.value === '' && !this.state.file}
                    className="btn-sm btn-outline-primary">
              send
            </button>
          </div>

        </form>
      </div>
    );
  }
}
