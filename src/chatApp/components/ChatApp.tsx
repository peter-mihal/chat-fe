import * as React from 'react';
import 'bootstrap/dist/js/bootstrap.bundle';
import {
  Route,
  Switch
} from 'react-router';
import { ToastContainer } from 'react-toastify';
import { ChannelRoutes } from '../containers/content/react-routing/ChannelRoutes';
import { LoginContainer } from '../containers/Login';
import {
  LocationComponentProps,
  withLocation
} from '../utils/withLocation';

class ChatApp extends React.PureComponent<LocationComponentProps> {
  static displayName = 'Container';

  render() {
    return (
      <div className="container-fluid">
        <ToastContainer autoClose={2000}/>

        <div className="row h-100">
          <Switch>
            <Route exact path="/" component={LoginContainer}/>
            <Route path="/channels" component={ChannelRoutes}/>
          </Switch>
        </div>
      </div>
    );
  }
}

const ChatAppWithLocation = withLocation(ChatApp);

export { ChatAppWithLocation as ChatApp };
