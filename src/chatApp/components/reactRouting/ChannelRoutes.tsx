import * as React from 'react';
import 'bootstrap/dist/js/bootstrap.bundle';
import {Redirect, Route} from 'react-router';
import { Content } from '../../containers/content/react-routing/Content';

import { Sidebar } from '../../containers/content/react-routing/Sidebar';
import { IUser } from '../../models/IChatApp';

export interface IChannelRoutesStateProps {
  readonly isLoading: boolean;
  readonly isSaving: boolean;
  readonly user: IUser;
}

export interface IChannelRoutesDispatchProps {
  readonly loadChannels: (userEmail: string) => void;
}

type ChannelRoutesProps = IChannelRoutesStateProps & IChannelRoutesDispatchProps;

export class ChannelRoutes extends React.PureComponent<ChannelRoutesProps> {
  componentDidMount() {
    this.props.loadChannels(this.props.user.email);
  }

  render() {
    if (!this.props.user.isLoggedIn) {
      return (
        <Redirect to="/"/>
      );
    } else {
      return (
        <div className="container-fluid">
          <div className="row h-100">
            <Sidebar/>

            <Route exact path="/channels" render={() =>
              <div className="d-flex align-items-center justify-content-center col-lg-7 col-sm-12">
                <h2 className="d-flex flex-column align-items-center">
                  <div>
                    <small className="text-muted">Welcome</small>
                    <span className="text-primary"> to chat.</span>
                  </div>
                  <small className="text-muted">Select desired channel.</small>
                </h2>
              </div>
            }/>

            <Route exact path={`/channels/:id`} component={Content}/>
          </div>
        </div>
      );
    }
  }
}
