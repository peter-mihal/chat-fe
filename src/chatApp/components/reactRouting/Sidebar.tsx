import * as Immutable from 'immutable';
import * as React from 'react';
import { Link } from 'react-router-dom';
import { IUser } from '../../models/IChatApp';
import { withLocation } from '../../utils/withLocation';
import { UserProfileContainer } from '../../containers/UserProfile';
import { ChannelContainer } from '../../containers/Channel';
import { AddChannelModal } from '../AddChannelModal';

export interface ISidebarStateProps {
  readonly channelsIds: Immutable.List<Uuid>;
  readonly user: IUser;
  readonly isLoading: boolean;
}

export interface ISidebarDispatchProps {
  readonly onCreateChannel: (name: string, userEmail: string) => void;
  readonly onDeleteChannel: (id: Uuid, userEmail: string) => void;
  readonly onLogout: () => void;
}

type IProps = ISidebarStateProps & ISidebarDispatchProps;

// TODO stiahnut usera po prihlasnei
class Sidebar extends React.PureComponent<IProps> {
  static displayName = 'Sidebar';

  constructor(props: any) {
    super(props);
  }

  render(): JSX.Element {
    return (
      <div className="col-lg-4 col-sm-12 sidebar d-flex flex-column justify-content-between">

        <div className="header h-10 content-header-min-height border-bottom">
          <div className="h-100 d-flex align-items-center">
            <div className="dropdown">
              <div className="dropdown-toggle hoverable"
                   data-toggle="dropdown">
                <img className="profile-pic"
                     src={this.props.user.customData.photoUri ? this.props.user.customData.photoUri : 'https://www.qualiscare.com/wp-content/uploads/2017/08/default-user.png'}/>
              </div>

              <div className="dropdown-menu">
                <a className="dropdown-item pointer"
                   data-toggle="modal"
                   data-target="#profileModal">
                  Profile
                </a>
                <Link onClick={this.props.onLogout}
                      className="dropdown-item pointer"
                      to="/">Log out</Link>
              </div>
            </div>
            <div className="d-flex flex-column ml-2 mr-auto">
              <span>{this.props.user.customData.username}</span>
              <small>{this.props.user.email}</small>
            </div>

            <button type="button"
                    className="btn btn-sm btn-secondary"
                    data-toggle="modal"
                    data-target="#addChannelModal">
              Add channel
            </button>
          </div>
        </div>

        <div className="channels h-90">
          {this.props.channelsIds.map((id: Uuid, index: number) => (
            <ChannelContainer
              key={id}
              id={id}
              index={index}
              onDelete={() => this.props.onDeleteChannel(id, this.props.user.email)}
            />
          ))
          }
        </div>

        <div className="sidebar-modal">
          <UserProfileContainer/>
          <AddChannelModal onSave={(name: string) => {
            this.props.onCreateChannel(name, this.props.user.email);
          }}/>
        </div>

      </div>
    );
  }
}

const SidebarWithLocation = withLocation(Sidebar);

export { SidebarWithLocation as Sidebar };
