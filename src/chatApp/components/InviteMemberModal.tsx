import * as React from 'react';

export interface IInviteMemberModalProps {
  readonly channelId: Uuid;
  readonly onInvite: (email: string) => void;
}

interface IInviteMemberModalState {
  readonly value: string;
}

export class InviteMemberModal extends React.PureComponent<IInviteMemberModalProps, IInviteMemberModalState> {

  constructor(props: IInviteMemberModalProps) {
    super(props);

    this.state = {
      value: '',
    };
  }

  private onValueChanged = (event: React.FormEvent<HTMLInputElement>) => {
    const {value} = event.currentTarget;
    this.setState(_ => ({ value }));
  };

  private onInvite = (event: React.FormEvent) => {
    event.preventDefault();
    const email = this.state.value;
    const closeBtn = document.getElementById('inviteMemberModalCloseBtn' + this.props.channelId);
    if (closeBtn) {
      closeBtn.click();
    }
    this.props.onInvite(email);
  };

  private onCancel = () => {
    this.setState(_ => ({ value: '' }));
  };

  render(): JSX.Element {
    return (
      <div className="modal fade"
           id={'inviteMemberModal' + this.props.channelId}
           role="dialog">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">
                Invite member
              </h5>
              <button
                type="button"
                id={'inviteMemberModalCloseBtn' + this.props.channelId}
                className="close"
                data-dismiss="modal"
                onClick={() => {
                  this.onCancel();
                }}>
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <form onSubmit={this.onInvite}>
              <div className="modal-body">
                <div className="form-group">
                  <input
                    value={this.state.value}
                    onChange={this.onValueChanged}
                    type="email"
                    className="form-control"
                    required
                    placeholder="Email"/>
                </div>
              </div>

              <div className="modal-footer">
                <button type="button"
                        className="btn btn-secondary"
                        data-dismiss="modal"
                        onClick={() => {
                          this.onCancel();
                        }}>
                  Close
                </button>
                <button type="submit"
                        className="btn btn-primary">
                  Invite
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}
