import * as React from 'react';
import { IChannel } from '../models/IChannel';
import {
  NavLink
} from 'react-router-dom';
import {InviteMemberModal} from './InviteMemberModal';
import {DOWN, UP} from '../constants/changeOrderDirections';

export interface IChannelDisplayProps {
  readonly channel: IChannel;
  readonly onDelete: () => void;
  readonly onEditStart: () => void;
  readonly onInviteMember: (email: string) => void;
  readonly onChangeOrder: (direction: Direction) => void;
}

export class ChannelDisplay extends React.PureComponent<IChannelDisplayProps> {
  render() {
    const {channel} = this.props;
    // @ts-ignore
    const checkActive = (match, location) => {
      // some additional logic to verify you are in the home URI
      if (!location) { return false; }
      const {pathname} = location;
      return pathname === '/';
    };

    return (
      <div>
        <div className="d-flex justify-content-between w-100">

          {/*<Link
         className="channel-link hoverable"
         to={{
         pathname: '/channels/' + channel.id,
         state: {title: channel.name}
         }}
         >
         {channel.name}
         </Link>*/}

          <NavLink
            activeClassName="active"
            isActive={checkActive}
            className="channel-link hoverable"
            to={{
              pathname: '/channels/' + channel.id,
              state: {title: channel.name}
            }}
          >
            {channel.name}
          </NavLink>

          <div className="d-flex justify-content-end">
            <span className="pl-2 pr-2">
              <span className="change-order-up hoverable pointer" onClick={() => { this.props.onChangeOrder(UP); }}>▲</span>
              <span className="change-order-down hoverable pointer" onClick={() => { this.props.onChangeOrder(DOWN); }}>▼</span>
            </span>

            <div className="dropdown">
              <span className="dropdown-toggle hoverable"
                    data-toggle="dropdown">
                  <i className="fa fa-cog"/>
              </span>
              <ul className="dropdown-menu">
                <li className="dropdown-item pointer" onClick={this.props.onEditStart}>Change name</li>
                <li className="dropdown-item pointer"
                    data-toggle="modal"
                    data-target={'#inviteMemberModal' + this.props.channel.id}>Invite member</li>
                <li className="dropdown-item pointer" onClick={this.props.onDelete}>Delete</li>
              </ul>
            </div>
          </div>
        </div>

        <div className="sidebar-modal">
          <InviteMemberModal
            channelId={this.props.channel.id}
            onInvite={(email: string) => {
            this.props.onInviteMember(email);
          }}/>
        </div>

      </div>
    );
  }
}
