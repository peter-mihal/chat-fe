import * as React from 'react';
import {IChannel} from '../models/IChannel';
import {ChannelDisplay} from './ChannelDisplay';
import {ChannelEdit} from './ChannelEdit';

export interface IChannelOwnProps {
  readonly id: Uuid;
  readonly index: number;
  readonly onDelete: (isActive: boolean) => void;
}

export interface IChannelStateProps {
  readonly channel: IChannel;
}

export interface IChannelDispatchProps {
  readonly onUpdate: (newName: string) => void;
  readonly onInviteMember: (email: string) => void;
  readonly onChangeOrder: (direction: Direction) => void;
}

interface WithRouterProps {
  location?: any;
  history?: any;
}

type ChannelProps = IChannelOwnProps & IChannelStateProps & IChannelDispatchProps & WithRouterProps;

interface IChannelState {
  readonly isBeingEdited: boolean;
  readonly isActive: boolean;
}

export class Channel extends React.PureComponent<ChannelProps, IChannelState> {

  constructor(props: ChannelProps) {
    super(props);
    this.state = {
      isBeingEdited: false,
      isActive: false,
    };
  }

  componentWillMount() {
    this.setActive();
  }
  componentWillUpdate() {
    this.setActive();
  }

  setActive = () => {
    if (this.props.location && this.props.location.pathname === ('/channels/' + this.props.channel.id)) {
      this.setState(() => {
        return {isActive: true};
      });
    }
    else {
      this.setState(() => {
        return {isActive: false};
      });
    }
  };

  private delete = () => {
    if (this.state.isActive) {
      this.props.history.push('/channels');
    }
    this.props.onDelete(false);
  };

  private startEditing = () => {
    this.setState(() => {
      return {isBeingEdited: true};
    });
  };

  private endEditing = () => {
    this.setState(() => {
      return {isBeingEdited: false};
    });
  };

  render() {
    return (
        <div key={this.props.index} className={'channel pt-2 ' + (this.state.isActive ? 'active' : '')}>
          {this.state.isBeingEdited
            ? <ChannelEdit
              channel={this.props.channel}
              onEditCanceled={this.endEditing}
              onUpdate={(newName: string) => {
                this.props.onUpdate(newName);
                this.endEditing();
              }}
            />
            : <ChannelDisplay
              channel={this.props.channel}
              onDelete={this.delete}
              onEditStart={this.startEditing}
              onInviteMember={this.props.onInviteMember}
              onChangeOrder={this.props.onChangeOrder}
            />
          }
        </div>
    );
  }
}
