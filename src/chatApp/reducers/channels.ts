import * as Immutable from 'immutable';
import { combineReducers } from 'redux';
import {
  CHAT_APP_CHANNELS_LOADING_SUCCESS,
  CHAT_APP_CHANNEL_DELETE_SUCCESS,
  CHAT_APP_CHANNEL_CREATE_SUCCESS,
  CHAT_APP_CHANNEL_UPDATE_SUCCESS, CHAT_APP_CHANNEL_CHANGE_ORDER,
} from '../constants/actionTypes';
import { IChannel } from '../models/IChannel';
import { IChannels } from '../models/IChatApp';

const byId = (prevState: Immutable.Map<Uuid, IChannel> = Immutable.Map<Uuid, IChannel>(), action: Action): Immutable.Map<Uuid, IChannel> => {
  switch (action.type) {

    case CHAT_APP_CHANNELS_LOADING_SUCCESS:
    case CHAT_APP_CHANNEL_DELETE_SUCCESS:
      return Immutable.Map(action.payload.channels.map((channel: IChannel) => [channel.id, channel]));

    case CHAT_APP_CHANNEL_CREATE_SUCCESS: {
      const {channel} = action.payload;
      return prevState.set(channel.id, channel);
    }

    case CHAT_APP_CHANNEL_UPDATE_SUCCESS: {
      const {channel} = action.payload;
      return prevState.set(channel.id, channel);
    }

    default:
      return prevState;
  }
};

const allIds = (prevState: Immutable.List<Uuid> = Immutable.List<Uuid>(), action: Action): Immutable.List<Uuid> => {
  switch (action.type) {

    case CHAT_APP_CHANNELS_LOADING_SUCCESS:
    case CHAT_APP_CHANNEL_DELETE_SUCCESS:
      return Immutable.List(action.payload.channels.map((channel: IChannel) => channel.id));

    case CHAT_APP_CHANNEL_CREATE_SUCCESS:
      return prevState.push(action.payload.channel.id);

    case CHAT_APP_CHANNEL_CHANGE_ORDER:
      const {index, direction} = action.payload;
      const id = prevState.get(index);
      const secondId = prevState.get(index + direction);
      if (id && secondId && (index + direction >= 0)) {
        prevState = prevState.set(index, secondId);
        prevState = prevState.set(index + direction, id);
      }
      return prevState;

    default:
      return prevState;
  }
};

export const channels = combineReducers<IChannels>({
  allIds,
  byId,
});
