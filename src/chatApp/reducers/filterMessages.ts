import {
  CHAT_APP_MESSAGES_FILTER,
} from '../constants/actionTypes';

export const messagesFilter = (prevState: string, action: Action): string => {
  switch (action.type) {
    case CHAT_APP_MESSAGES_FILTER:
      return action.payload.filter.toLowerCase();

    default:
      return prevState;
  }
};
