import {
  CHAT_APP_CHANNELS_LOADING_STARTED,
  CHAT_APP_CHANNELS_LOADING_SUCCESS,
  CHAT_APP_LOGIN_FAIL,
  CHAT_APP_LOGIN_STARTED,
  CHAT_APP_LOGIN_SUCCESS,
  CHAT_APP_MESSAGES_LOADING_STARTED,
  CHAT_APP_MESSAGES_LOADING_SUCCESS,
  CHAT_APP_REGISTER_FAIL,
  CHAT_APP_REGISTER_STARTED,
  CHAT_APP_REGISTER_SUCCESS
} from '../constants/actionTypes';

export const isLoading = (prevState = false, action: Action): boolean => {
  switch (action.type) {
    // LOADING START
    case CHAT_APP_MESSAGES_LOADING_STARTED:
      return true;
    case CHAT_APP_CHANNELS_LOADING_STARTED:
      return true;
    case CHAT_APP_LOGIN_STARTED:
      return true;
    case CHAT_APP_REGISTER_STARTED:
      return true;

    // LOADING END
    case CHAT_APP_MESSAGES_LOADING_SUCCESS:
      return false;
    case CHAT_APP_CHANNELS_LOADING_SUCCESS:
      return false;
    case CHAT_APP_LOGIN_SUCCESS:
      return false;
    case CHAT_APP_LOGIN_FAIL:
      return false;
    case CHAT_APP_REGISTER_FAIL:
      return false;
    case CHAT_APP_REGISTER_SUCCESS:
      return false;

    default:
      return prevState;
  }
};
