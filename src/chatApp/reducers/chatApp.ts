import { IChatApp } from '../models/IChatApp';
import { messagesFilter } from './filterMessages';
import { isLoading } from './isLoading';
import { isSaving } from './isSaving';
import { messages } from './messages';
import { channels } from './channels';
import { user } from './user';
import { users } from './users';

export const chatApp = (prevState = {} as IChatApp, action: Action): IChatApp => ({
  messages: messages(prevState.messages, action),
  isLoading: isLoading(prevState.isLoading, action),
  isSaving: isSaving(prevState.isSaving, action),
  channels: channels(prevState.channels, action),
  user: user(prevState.user, action),
  messagesFilter: messagesFilter(prevState.messagesFilter, action),
  users: users(prevState.users, action),
});
