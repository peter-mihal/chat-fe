import { combineReducers } from 'redux';
import { IState } from '../../common/IState';
import { chatApp } from './chatApp';

export const root = combineReducers<IState>({chatApp});
