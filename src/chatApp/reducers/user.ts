import {
  CHAT_APP_USER_PROFILE_UPDATE_SUCCESS,
  CHAT_APP_LOGIN_SUCCESS,
  CHAT_APP_LOGOUT_SUCCESS,
  CHAT_APP_GET_USER_SUCCESS
} from '../constants/actionTypes';
import {
  IUser
} from '../models/IChatApp';

const initialState = {
  username: '',
  email: '',
  isLoggedIn: false,
  customData: {
    username: '',
    photoUri: ''
  }
};

export const user = (prevState: IUser = initialState, action: Action): IUser => {
  switch (action.type) {
    case CHAT_APP_LOGIN_SUCCESS: {
      const usr = action.payload.user;
      return Object.assign({}, prevState, {...usr});
    }

    case CHAT_APP_LOGOUT_SUCCESS: {
      return action.payload.user;
    }

    case CHAT_APP_USER_PROFILE_UPDATE_SUCCESS: {
      const usr = action.payload.user;
      return Object.assign({}, prevState, {...usr});
    }

    case CHAT_APP_GET_USER_SUCCESS: {
      const usr = action.payload.user;
      return Object.assign({}, prevState, {...usr});
    }

    default:
      return prevState;
  }
};
