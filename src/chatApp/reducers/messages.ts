import * as Immutable from 'immutable';
import { combineReducers } from 'redux';
import {
  CHAT_APP_MESSAGE_CREATE_SUCCESS,
  CHAT_APP_MESSAGE_DELETE_COMPLETE_SUCCESS,
  CHAT_APP_MESSAGE_UPDATE_SUCCESS,
  CHAT_APP_MESSAGES_LOADING_SUCCESS
} from '../constants/actionTypes';
import { IMessages } from '../models/IChatApp';
import { IMessage } from '../models/IMessage';

const byId = (prevState = Immutable.Map<Uuid, IMessage>(), action: Action): Immutable.Map<Uuid, IMessage> => {
  switch (action.type) {
    case CHAT_APP_MESSAGES_LOADING_SUCCESS:
    case CHAT_APP_MESSAGE_DELETE_COMPLETE_SUCCESS:
      return Immutable.Map(action.payload.messages.map((item: IMessage) => [item.id, item]));

    case CHAT_APP_MESSAGE_CREATE_SUCCESS: {
      return prevState.set(action.payload.message.id, action.payload.message);
    }

    case CHAT_APP_MESSAGE_UPDATE_SUCCESS: {
      const {message} = action.payload;
      return prevState.set(message.id, {...message});
    }

    default:
      return prevState;
  }
};

const allIds = (prevState: Immutable.List<Uuid> = Immutable.List(), action: Action): Immutable.List<Uuid> => {
  switch (action.type) {

    case CHAT_APP_MESSAGES_LOADING_SUCCESS:
    case CHAT_APP_MESSAGE_DELETE_COMPLETE_SUCCESS:
      const mssgs = action.payload.messages;
      mssgs.sort((m1: IMessage, m2: IMessage) => {
        return Date.parse(m1.createdAt) - Date.parse(m2.createdAt);
      });
      return Immutable.List(mssgs.map((item: IMessage) => item.id));

    case CHAT_APP_MESSAGE_CREATE_SUCCESS:
      return prevState.push(action.payload.message.id);

    default:
      return prevState;
  }
};

export const messages = combineReducers<IMessages>({
  allIds,
  byId,
});
