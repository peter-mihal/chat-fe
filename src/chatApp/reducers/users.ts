import {
  CHAT_APP_LOGIN_SUCCESS
} from '../constants/actionTypes';
import {
  IUser
} from '../models/IChatApp';

export const users = (prevState: IUser[] = [], action: Action): IUser[] => {
  switch (action.type) {
    case CHAT_APP_LOGIN_SUCCESS: {
      const usrs = action.payload.users;
      return Object.assign({}, prevState, {...usrs});
    }

    default:
      return prevState;
  }
};
