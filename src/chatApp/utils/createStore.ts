import {
  applyMiddleware,
  compose,
  createStore as createReduxStore
} from 'redux';
import thunk from 'redux-thunk';
import { IState } from '../../common/IState';
import { root } from '../reducers/root';
import {
  Action,
  Store
} from '../../@types/redux';


const composeEnhancers = (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const middleware = [thunk];

const store = createReduxStore<IState, Action, never, never>(root, composeEnhancers(
  applyMiddleware(...middleware)
));

export const createStore = (): Store => store;
