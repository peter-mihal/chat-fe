export interface IFile {
  readonly createdBy: string;
  readonly extension: string;
  readonly fileSize: number;
  readonly id: string;
  readonly name: string;
  fileUri: string;
}

export interface IMessageData {
  likes: number;
  dislikes: number;
  file: IFile | undefined;
  channelId: string;
  userPhoto: string;
}

export interface IMessage {
  readonly id: Uuid;
  readonly value: string;
  readonly createdAt: string;
  readonly createdBy: string;
  readonly updatedAt: string;
  readonly updatedBy: string;
  readonly customData: IMessageData;
}
