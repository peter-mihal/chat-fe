import * as Immutable from 'immutable';

export interface IChannelData {
  users: Immutable.Set<string>;
}

export interface IChannel {
  readonly id: Uuid;
  readonly name: string;
  readonly customData: IChannelData;
}
