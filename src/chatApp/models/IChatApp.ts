import * as Immutable from 'immutable';
import { IMessage } from './IMessage';
import { IChannel } from './IChannel';

export interface IMessages {
  allIds: Immutable.List<Uuid>;
  byId: Immutable.Map<Uuid, IMessage>;
}

export interface IUserData {
  username: string;
  photoUri: string | undefined;
}

export interface IChannels {
  allIds: Immutable.List<Uuid>;
  byId: Immutable.Map<Uuid, IChannel>;
}

export interface IUser {
  email: string;
  username: string;
  isLoggedIn: boolean;
  customData: IUserData;
}

export interface IUserDropdown {
  id: string;
  value: string;
  img: string | undefined;
}

export interface IChatApp {
  messages: IMessages;
  isLoading: boolean;
  isSaving: boolean;
  channels: IChannels;
  user: IUser;
  messagesFilter: string;
  users: IUser[];
}
