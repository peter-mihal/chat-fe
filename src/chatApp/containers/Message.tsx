import { connect } from 'react-redux';
import { Dispatch } from 'redux';
// @ts-ignore
import { IState } from '../../common/IState';
import {
  deleteMessage
} from '../actions/deleteMessage';
import {
  dislikeMessage,
  /*
   dislikeMessage,
   */
  likeMessage
} from '../actions/updateMessage';
import {
  IMessageDispatchProps,
  IMessageOwnProps,
  IMessageStateProps,
  Message
} from '../components/Message';
import { IMessage } from '../models/IMessage';

const mapStateToProps = (state: IState, ownProps: IMessageOwnProps): IMessageStateProps => {
  return {
    // @ts-ignore
    message: state.chatApp.messages.byId.get<IMessage>(ownProps.id)
  };
};

const mapDispatchToProps = (dispatch: Dispatch, ownProps: IMessageOwnProps) => {
  return {
    onDelete: () => dispatch(deleteMessage(ownProps.id)),
    like: () => dispatch(likeMessage(ownProps.id)),
    dislike: () => dispatch(dislikeMessage(ownProps.id)),
  };
};

export const MessageContainer = connect<IMessageStateProps, IMessageDispatchProps, IMessageOwnProps>(mapStateToProps, mapDispatchToProps)(Message);
