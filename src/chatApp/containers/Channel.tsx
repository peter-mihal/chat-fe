import {connect} from 'react-redux';
import * as Immutable from 'immutable';
import {Dispatch} from 'redux';
import {IState} from '../../common/IState';
import {Channel, IChannelDispatchProps, IChannelOwnProps, IChannelStateProps} from '../components/Channel';
import {IChannel} from '../models/IChannel';
import {changeChannelName, inviteMemberToChannel} from '../actions/updateChannel';
import {changeChannelOrder} from '../actions/actionCreators';
import {withRouter} from 'react-router';

const mapStateToProps = (state: IState, ownProps: IChannelOwnProps): IChannelStateProps => {
  return {
    channel: state.chatApp.channels.byId.get<IChannel>(ownProps.id, {
      id: '?',
      name: '?',
      customData: {
        users: Immutable.Set([]),
      },
    }),
  };
};

const mapDispatchToProps = (dispatch: Dispatch, ownProps: IChannelOwnProps): IChannelDispatchProps => {
  return {
    onUpdate: (newName: string) => dispatch(changeChannelName(ownProps.id, newName)),
    onInviteMember: (email: string) => dispatch(inviteMemberToChannel(ownProps.id, email)),
    onChangeOrder: (direction: Direction) => dispatch(changeChannelOrder(ownProps.index, direction)),
  };
};

export const ChannelContainer = withRouter(connect<IChannelStateProps, IChannelDispatchProps, IChannelOwnProps>(mapStateToProps, mapDispatchToProps)(Channel));
