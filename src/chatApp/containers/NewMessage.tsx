import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { IState } from '../../common/IState';
import {
  createFileAction,
  createMessageAction
} from '../actions/createMessage';
import {
  INewMessageDispatchProps,
  INewMessageStateProps,
  NewMessage
} from '../components/NewMessage';
import { IUser } from '../models/IChatApp';
import { IFile } from '../models/IMessage';

const mapStateToProps = (state: IState): INewMessageStateProps => {
  return {
    user: state.chatApp.user,
    users: state.chatApp.users
  };
};

const mapDispatchToProps = (dispatch: Dispatch): INewMessageDispatchProps => {
  return {
    onMessageAdd: (text: string, user: IUser, channelId: string, file?: IFile) => dispatch(createMessageAction(text, user, channelId, file  ? file : undefined)),
    onFileAdd: (file: any) => dispatch(createFileAction(file)),
  };
};

export const NewMessageContainer = connect(mapStateToProps, mapDispatchToProps)(NewMessage);
