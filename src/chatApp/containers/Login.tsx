import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { IState } from '../../common/IState';
import {
  loginAction,
  registerAction
} from '../actions/authUser';
import {
  Login,
  ILoginDispatchProps,
  ILoginStateProps
} from '../components/Login';


const mapStateToProps = (state: IState): ILoginStateProps => {

  return {
    user: state.chatApp.user,
    isLoading: state.chatApp.isLoading
  };
};

const mapDispatchToProps = (dispatch: Dispatch): ILoginDispatchProps => {
  return {
    onLogin: (email: string) => dispatch(loginAction(email)),
    onRegister: (email: string) => dispatch(registerAction(email)),
  };
};

export const LoginContainer = connect(mapStateToProps, mapDispatchToProps)(Login);
