import * as React from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { loadChannels } from '../../../actions/loadChannels';
import {
  IChannelRoutesDispatchProps,
  IChannelRoutesStateProps,
  ChannelRoutes
} from '../../../components/reactRouting/ChannelRoutes';
import { withLocation } from '../../../utils/withLocation';
import {IState} from '../../../../common/IState';

const mapStateToProps = (state: IState): IChannelRoutesStateProps => {
  return {
    isLoading: state.chatApp.isLoading,
    isSaving: state.chatApp.isSaving,
    user: state.chatApp.user,
  };
};

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    loadChannels: (userEmail: string) => {
      dispatch(loadChannels(userEmail));
    }
  };
};

const ChannelRoutesConnected = connect<IChannelRoutesStateProps, IChannelRoutesDispatchProps>(mapStateToProps, mapDispatchToProps)(ChannelRoutes);

const ChannelRoutesWithRouter: React.ComponentType = withLocation(ChannelRoutesConnected);

export { ChannelRoutesWithRouter as ChannelRoutes };
