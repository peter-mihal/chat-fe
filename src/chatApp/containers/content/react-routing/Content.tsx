import * as Immutable from 'immutable';
import * as React from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { IState } from '../../../../common/IState';
import { setMessagesFilter } from '../../../actions/actionCreators';
import { loadMessages } from '../../../actions/loadMessages';
import {
  Content,
  IContentStateProps
} from '../../../components/Content';
import { withLocation } from '../../../utils/withLocation';

const getMessages = (state: IState, filter: string): Immutable.List<Uuid> => {
  if (filter && filter.length) {
    // @ts-ignore
    return state.chatApp.messages.allIds.filter((id: Uuid) => state.chatApp.messages.byId.get(id).value.toLowerCase().indexOf(filter) >= 0 );
  } else {
    return  state.chatApp.messages.allIds;
  }
};

const mapStateToProps = (state: IState): IContentStateProps => {
  return {
    messagesIds: getMessages(state, state.chatApp.messagesFilter),
    isLoading: state.chatApp.isLoading
  };
};

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    loadMessages: (channelId: string) => {
      dispatch(loadMessages(channelId));
    },
    filterMessages: (searchText: string) => {
      dispatch(setMessagesFilter(searchText));
    }
  };
};

const ContentConnected = connect(mapStateToProps, mapDispatchToProps)(Content);

const ContentWithRouter: React.ComponentType = withLocation(ContentConnected);

export { ContentWithRouter as Content };

