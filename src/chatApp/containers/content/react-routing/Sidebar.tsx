import * as Immutable from 'immutable';
import * as React from 'react';
import { connect } from 'react-redux';
import { IState } from '../../../../common/IState';
import { logoutUser } from '../../../actions/authUser';
import {
  ISidebarDispatchProps,
  ISidebarStateProps,
  Sidebar
} from '../../../components/reactRouting/Sidebar';
import { withLocation } from '../../../utils/withLocation';
import { Dispatch } from 'redux';
import { createChannel } from '../../../actions/createChannel';
import { deleteChannel } from '../../../actions/deleteChannel';

const getChannelsIds = (state: IState): Immutable.List<Uuid> => {
  return state.chatApp.channels.allIds;
};

const mapStateToProps = (state: IState): ISidebarStateProps => {
  return {
    channelsIds: getChannelsIds(state),
    user: state.chatApp.user,
    isLoading: state.chatApp.isLoading
  };
};

const mapDispatchToProps = (dispatch: Dispatch): ISidebarDispatchProps => {
  return {
    onCreateChannel: (name: string, userEmail: string) => dispatch(createChannel(name, userEmail)),
    onDeleteChannel: (id: Uuid, userEmail: string) => dispatch(deleteChannel(id, userEmail)),
    onLogout: () => dispatch(logoutUser())
  };
};

const SidebarConnected = connect<ISidebarStateProps, ISidebarDispatchProps>(mapStateToProps, mapDispatchToProps)(Sidebar);

const SidebarWithRouter: React.ComponentType = withLocation(SidebarConnected);

export { SidebarWithRouter as Sidebar };
