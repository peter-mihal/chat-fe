import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { IState } from '../../common/IState';
import { createFileAction } from '../actions/createMessage';
import {
  updateUser
} from '../actions/updateUserProfile';
import {
  IUserProfileDispatchProps,
  UserProfile
} from '../components/UserProfile';
import {
  IUserData
} from '../models/IChatApp';
import { IUserState } from '../components/UserProfile';
import { IFile } from '../models/IMessage';

const mapStateToProps = (state: IState): IUserState => {
  return {data: state.chatApp.user.customData};
};

const mapDispatchToProps = (dispatch: Dispatch): IUserProfileDispatchProps => {
  return {
    onProfileChanged: (data: IUserData, file?: IFile) => dispatch(updateUser(data, file)),
    onFileAdd: (file: any) => dispatch(createFileAction(file))
  };
};

export const UserProfileContainer = connect(mapStateToProps, mapDispatchToProps)(UserProfile);
