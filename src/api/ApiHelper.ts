export interface IApiHelper {
  get(url: string, withAppId: boolean): Promise<any>;
  post(url: string, params: any, withAppId: boolean, isFormData?: boolean): Promise<any>;
  put(url: string, params: Object, withAppId: boolean): Promise<any>;
  delete(url: string, withAppId: boolean): Promise<any>;
}

export class ApiHelper implements IApiHelper {
  private appId = 'cd9226e5-edae-49d6-8ac6-f9cf9dd82eb7/';
  private apiUrl = 'https://pv247messaging.azurewebsites.net/api/v2/';

  public get(url: string, withAppId: boolean): Promise<any> {
    const endpoint = (withAppId ? this.apiUrl + (url.split('/')[0] === 'user' ? '' : 'app/') + this.appId : this.apiUrl) + url;

    return fetch(endpoint, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + localStorage.getItem('token'),
      }
    }).then(res => {
      if (!res.ok) {
        throw Error(res.statusText);
      }
      return res.json();
    });
  }

  public post(url: string, params: any, withAppId: boolean, isFormData?: boolean): Promise<any> {
    const endpoint = (withAppId ? this.apiUrl + (url === 'user' ? '' : 'app/') + this.appId : this.apiUrl) + url;
    return fetch(endpoint, {
      method: 'POST',
      headers: isFormData ? {
        Authorization: 'Bearer ' + localStorage.getItem('token'),
      } : {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + localStorage.getItem('token'),
      },
      body: isFormData ? params : JSON.stringify(params)
    }).then(res => {
      if (!res.ok) {
        throw Error(res.statusText);
      }
      return res.json();
    });
  }

  public put(url: string, params: Object, withAppId: boolean): Promise<any> {
    const endpoint = (withAppId ? this.apiUrl + (url.split('/')[0] === 'user' ? '' : 'app/') + this.appId : this.apiUrl) + url;

    return fetch(endpoint, {
      method: 'PUT',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + localStorage.getItem('token'),
      },
      body: JSON.stringify(params)
    }).then(res => {
      if (!res.ok) {
        throw Error(res.statusText);
      }
      return res.json();
    });
  }

  public delete(url: string, withAppId: boolean): Promise<any> {
    const endpoint = (withAppId ? this.apiUrl + 'app/' + this.appId : this.apiUrl) + url;

    return fetch(endpoint, {
      method: 'DELETE',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + localStorage.getItem('token'),
      }
    }).then(res => {
      if (!res.ok) {
        throw Error(res.statusText);
      }
    });
  }
}
